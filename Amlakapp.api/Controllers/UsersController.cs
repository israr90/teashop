﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using System.Data;
using System;
using Amlakapp.api.Models;
using Amlakapp.api.Service;
using Amlakapp.Core;
using Amlakapp.Entities;

namespace Amlakapp.api.Controllers
{
    /// <summary>
    /// The Notes instance resource
    /// </summary>
    [RoutePrefix("Api/Users")]
    public class UsersController : ApiController
    {
        db_amlakEntities db = new db_amlakEntities();
        //db_amlakEntities db = new db_amlakEntities();

        #region RegisterUser
        /// <summary>
        /// Create new customer
        /// </summary>
        /// <param name="RegisterCompanyRequest">RegisterCompanyRequest object which contains Header and Customer object</param>
        /// <returns>Returns Result object</returns>
        [AllowAnonymous]
        [HttpPost, Route("Register")]
        [ResponseType(typeof(RegisterUserResponse))]
        public IHttpActionResult Register([FromBody]RegisterViewModel pRegisterUserRequest)
        {
            RegisterUserResponse response = new RegisterUserResponse();
            Result ErrorResult = Utils.ValidateUserRequest(pRegisterUserRequest);
            if (ErrorResult.Rflag == 1)
            {
                AuthRepository authRepository = new AuthRepository();
                var result = authRepository.RegisterUser(pRegisterUserRequest, true);
                if (result.Rflag == 1)
                {
                    string myID = DBUtils.executeSqlGetSingle("SELECT UserID FROM [dbo].[Users] WHERE email= '" + pRegisterUserRequest.Email + "'");
                    if (myID.Trim() != "")
                    {
                        AppToken myToken = TokenServices.GenerateToken(myID.Trim());
                        if (myToken != null && myToken.AuthToken.Trim() != "")
                        {
                            Utils.GetResult(ref response.Result, "0");
                            response.AuthToken = myToken.AuthToken;
                            response.Data = AppUser.AppGetUserProfile(myID);
                        }
                        else
                        {
                            Utils.GetResult(ref response.Result, "103");
                        }
                    }
                    else
                    {
                        Utils.GetResult(ref response.Result, "102");
                    }
                }
                response.Result = result;
            }
            else
            {
                Utils.GetResult(ref response.Result, Common.toString(ErrorResult.Code));
            }
            return Ok(response);
        }
        #endregion
        #region Login
        [HttpPost, Route("Login")]
        [ResponseType(typeof(PostUserResponse))]
        public IHttpActionResult Login([FromBody]PostUserRequest pPostUserRequest)
        {
            PostUserResponse myPostUserResponse = new PostUserResponse();
            if (pPostUserRequest != null && pPostUserRequest.AuthType > 0)
            {
                if (pPostUserRequest.AuthType == 1) // username password
                {
                    #region SignIn old
                    if (Utils.ValidateAuthKeyWithoutToken(pPostUserRequest))
                    {
                        if (pPostUserRequest.Email != null && pPostUserRequest.Password != null)
                        {
                            if (AppUser.ValidateUser(pPostUserRequest.Email, pPostUserRequest.Password))
                            {
                                string myID = DBUtils.executeSqlGetSingle("SELECT UserID FROM [dbo].[Users] WHERE email= '" + pPostUserRequest.Email + "'");
                                if (myID.Trim() != "")
                                {
                                    AppToken myToken = TokenServices.GenerateToken(myID.Trim());
                                    if (myToken != null && myToken.AuthToken.Trim() != "")
                                    {
                                        Utils.GetResult(ref myPostUserResponse.result, "0");
                                        myPostUserResponse.AuthToken = myToken.AuthToken;
                                        myPostUserResponse.data = AppUser.AppGetUserProfile(myID);
                                    }
                                    else
                                    {
                                        Utils.GetResult(ref myPostUserResponse.result, "103");
                                    }
                                }
                                else
                                {
                                    Utils.GetResult(ref myPostUserResponse.result, "102");
                                }
                            }
                            else
                            {
                                Utils.GetResult(ref myPostUserResponse.result, "202");
                            }
                        }
                        else
                        {
                            Utils.GetResult(ref myPostUserResponse.result, "202");
                        }
                    }
                    else
                    {
                        Utils.GetResult(ref myPostUserResponse.result, "100");
                    }
                    #endregion
                }
                else
                {
                    Utils.GetResult(ref myPostUserResponse.result, "100B");
                }
            }
            else
            {
                Utils.GetResult(ref myPostUserResponse.result, "100B");
            }
            return Ok(myPostUserResponse);
        }
        #endregion
        #region LogOut
        [HttpPost, Route("LogOut")]
        [ResponseType(typeof(PostUserResponse))]
        public IHttpActionResult LogOut([FromBody]UserProfileRequest pPostUserRequest)
        {
            PostUserResponse myPostUserResponse = new PostUserResponse();
            if (Utils.ValidateAuthKey(pPostUserRequest))
            {
                AppToken myToken = TokenServices.GetToken(pPostUserRequest.AuthToken);
                if (myToken != null)
                {
                   bool isTrue= TokenServices.Kill(myToken.AuthToken);
                    if (isTrue)
                    {
                        Utils.GetResult(ref myPostUserResponse.result, "0");
                    }
                    
                }
                else
                {
                    Utils.GetResult(ref myPostUserResponse.result, "101");
                }
            }
            else
            {
                Utils.GetResult(ref myPostUserResponse.result, "100");
            }

            return Ok(myPostUserResponse);
        }
        #endregion
        #region Get User Profile
        /// <summary>
        /// Get User Profile
        /// </summary>
        /// <param name="GetUserProfileRequest">GetUserProfileRequest object</param>
        /// <returns>Returns Result object</returns>
        [AllowAnonymous]
        [HttpPost, Route("GetUserProfile")]
        [ResponseType(typeof(PostUserResponse))]
        public IHttpActionResult GetUserProfile([FromBody]UserProfileRequest pGetUserProfileRequest)
        {
            UserProfileResponse response = new UserProfileResponse();
            
            if (Utils.ValidateAuthKey(pGetUserProfileRequest))
            {
                AppToken myToken = TokenServices.GetToken(pGetUserProfileRequest.AuthToken);
                if (myToken != null)
                {
                    response.Data = AppUser.AppGetUserProfile(myToken.UserId);
                    Utils.GetResult(ref response.Result, "0");
                }
                else
                {
                    Utils.GetResult(ref response.Result, "101");
                }
            }
            else
            {
                Utils.GetResult(ref response.Result, "100");
            }

            return Ok(response);
        }

        #endregion

        #region Update User Profile

        /// <summary>
        /// Update User Profile
        /// </summary>
        /// <param name="EditUserRequest">EditRefusalRequest object</param>
        /// <returns>Returns Result object</returns>
        [AllowAnonymous]
        [HttpPost, Route("UpdateProfile")]
       // [ResponseHttpStatusCode(HttpStatusCode.Created)]
        [ResponseType(typeof(UserProfileResponse))]
        public IHttpActionResult UpdateProfile([FromBody]UpdateProfileRequest pUpdateUserRequest)
        {
            UserProfileResponse response = new UserProfileResponse();
            if (Utils.ValidateAuthKey(pUpdateUserRequest))
            {
                AppToken myToken = TokenServices.GetToken(pUpdateUserRequest.AuthToken);
                if (myToken != null)
                {
                    response = AppUser.UserUpdateProfile(pUpdateUserRequest, myToken.UserId);
                    if (response.Result.Code == 0)
                    {
                        Utils.GetResult(ref response.Result, "0");
                    }
                    else
                    {
                        Utils.GetResult(ref response.Result, response.Result.Code.ToString());
                    }
                }
                else
                {
                    Utils.GetResult(ref response.Result, "101");
                }
            }
            else
            {
                Utils.GetResult(ref response.Result, "100");
            }
            return Ok(response);


        }
        #endregion

        #region ForgotPassword
        /// <summary>
        ///  ForgotPassword
        /// </summary>
        /// <param name="ForgotPasswordRequest">ForgotPasswordRequest object</param>
        /// <returns>Returns Result object</returns>        
        [AllowAnonymous]
        [HttpPost, Route("ForgotPassword")]
       // [ResponseHttpStatusCode(HttpStatusCode.Created)]
        [ResponseType(typeof(ForgetPasswordResponse))]
        public IHttpActionResult ForgotPassword([FromBody]ForgetPasswordRequest pForgotPasswordRequest)
        {
            ForgetPasswordResponse response = new ForgetPasswordResponse();
            if (pForgotPasswordRequest != null)
            {
                if (!string.IsNullOrEmpty(pForgotPasswordRequest.Email))
                {
                    var email = db.Users.Where(m => m.Email == pForgotPasswordRequest.Email).FirstOrDefault();
                    if (email != null)
                    {
                        response= AppUser.ForgetPassword(pForgotPasswordRequest.Email);
                        if (response.Result.Code == 0)
                        {
                            Utils.GetResult(ref response.Result, "0");
                            response.Result.Description = "Dear customer an email is sent on your email " + pForgotPasswordRequest.Email;
                        }
                        else
                        {
                            Utils.GetResult(ref response.Result, response.Result.Code.ToString());
                        }
                    }
                    else
                    {
                        Utils.GetResult(ref response.Result, "3003");
                    }
                }
                else
                {
                    Utils.GetResult(ref response.Result, "3002");
                }
            }
            else
            {
                Utils.GetResult(ref response.Result, "3002");
            }

            return Ok(response);

        }

        #endregion

    }
}

