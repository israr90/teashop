using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using System.Data;
using System;
using Amlakapp.api.Models;
using Amlakapp.api.Service;
using Amlakapp.Core;
using Amlakapp.Entities;
namespace Amlakapp.api.Controllers
{ 
    #region GetProductList
    
    [RoutePrefix("Api/Product")]
    public class ProductController : ApiController
    {
        db_amlakEntities db = new db_amlakEntities();
       
        [HttpPost, Route("GetProductList")]
        [ResponseType(typeof(ProductListResponce))]
        
        public IHttpActionResult GetProductList([FromBody]ProductListRequest pGetProductList)
        {
            ProductListResponce response = new ProductListResponce();

            if (Utils.ValidateAuthKey(pGetProductList))
            {
                AppToken myToken = TokenServices.GetToken(pGetProductList.AuthToken);
                if (myToken != null)
                {
                    List<Product> plist = new List<Product>();
                    plist = db.Products.ToList();

                    response.Data = plist;
                    Utils.GetResult(ref response.Result, "0");
                }
                else
                {
                    Utils.GetResult(ref response.Result, "101");
                }
            }
            else
            {
                Utils.GetResult(ref response.Result, "100");
            }

            return Ok(response);
        }

#endregion
    }
}
