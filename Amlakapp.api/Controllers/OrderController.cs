using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using System.Data;
using System;
using Amlakapp.api.Models;
using Amlakapp.api.Service;
using Amlakapp.Core;
using Amlakapp.Entities;

namespace Amlakapp.api.Controllers
{
   
    #region GetOrderList
    [RoutePrefix("Api/Order")]
    public class OrderController : ApiController
    {
    

            //[HttpPost, Route("GetOrderList")]
            //[ResponseType(typeof(OrderListResponce))]

            //public IHttpActionResult GetOrderList([FromBody]OrderListRequest pGetOrderList)
            //{
            //OrderListResponce response = new OrderListResponce();

            //    if (Utils.ValidateAuthKey(pGetOrderList))
            //    {
            //        AppToken myToken = TokenServices.GetToken(pGetOrderList.AuthToken);
            //        if (myToken != null)
            //        {
            //            List<Order> plist = new List<Order>();
            //            plist = db.Orders.ToList();

            //            response.Data = plist;
            //            Utils.GetResult(ref response.Result, "0");
            //        }
            //        else
            //        {
            //            Utils.GetResult(ref response.Result, "101");
            //        }
            //    }
            //    else
            //    {
            //        Utils.GetResult(ref response.Result, "100");
            //    }

            //    return Ok(response);
            //}
        [HttpPost, Route("NewOrder")]
        [ResponseType(typeof(NewOrderResponse))]

        public IHttpActionResult NewOrder([FromBody]NewOrderRequest porderRequest)
        {
            NewOrderResponse response = new NewOrderResponse();

            if (Utils.ValidateAuthKey(porderRequest))
            {
                AppToken myToken = TokenServices.GetToken(porderRequest.AuthToken);
                if (myToken != null)
                {
                    porderRequest.OrderBy = myToken.UserId;
                    response = OrderService.NewOrder(porderRequest);
                    if (response.Data != null && response.Result.Code==0)
                    {
                        Utils.GetResult(ref response.Result, "0");
                    }
                    else
                    {
                        Utils.GetResult(ref response.Result, response.Result.Code.ToString());
                    }
                   
                }
                else
                {
                    Utils.GetResult(ref response.Result, "101");
                }
            }
            else
            {
                Utils.GetResult(ref response.Result, "100");
            }

            return Ok(response);
        }

      
    }
    #endregion
}

