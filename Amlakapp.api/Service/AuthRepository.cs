using Amlakapp.api.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Amlakapp.api.Service
{

    public class AuthRepository : IDisposable
    {
      //  private db_amlakEntities db = new db_amlakEntities();
        private ApplicationDbContext _ctx;
        private UserManager<IdentityUser> _userManager;
        private UserManager<ApplicationUser> _auserManager;

        public AuthRepository()
        {
            _ctx = new ApplicationDbContext();
            _userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(_ctx));

            _auserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(_ctx));

            //var provider = new DpapiDataProtectionProvider("Sample");
            //_auserManager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(provider.Create("EmailConfirmation"));

        }

        public async Task<IdentityResult> RegisterUser(RegisterViewModel registerViewModel)
        {
            ApplicationUser user = new ApplicationUser
            {
                UserName = registerViewModel.Email,
                Email = registerViewModel.Email,
                FirstName = registerViewModel.FirstName,
                LastName = registerViewModel.LastName,
                //CellPhone = registerViewModel.CellPhone,
                UserType = Convert.ToByte(50),
                AddedDate = DateTime.Now,
            };

            //var result = await _userManager.CreateAsync(user, userModel.Password);
            var result = await _userManager.CreateAsync(user, registerViewModel.Password);

            return result;
        }

      
        public Result RegisterUser(RegisterViewModel registerViewModel, bool async = true)
        {
            Result result = new Result();
            byte UserType = 50;
            if (registerViewModel.UserType != "" && registerViewModel.UserType != "0")
            {
                UserType = Convert.ToByte(registerViewModel.UserType);
            }
            ApplicationUser user = new ApplicationUser
            {
                UserName = registerViewModel.Email,
                Email = registerViewModel.Email,
                FirstName = registerViewModel.FirstName,
                LastName = registerViewModel.LastName,
                //CellPhone = registerViewModel.CellPhone,
                //UserType = UserType,
                UserType = (byte)50,
                AddedDate = DateTime.Now,
                Status = true
            };
            var UserResult = _userManager.Create(user, registerViewModel.Password);
            if (UserResult.Succeeded)
            {
                result.Rflag = 1;
                result.Message = "Congratulations! Your account created successfully using [" + registerViewModel.Email + "].";
            }
            else
            {
                result.Rflag = 0;
                result.Code = 1001;
                result.Message = UserResult.Errors.Take(1).SingleOrDefault();
            }
            return result;
        }
        public Result ForgotPassword(string pEmail)
        {
            Result result = new Result();
            try
            {
                var user = _auserManager.FindByEmail(pEmail);

                if (user != null)
                {
                    //For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    //Send an email with this link
                    //string code = _userManager.GeneratePasswordResetTokenAsync(user.Id);
                    var provider = new Microsoft.Owin.Security.DataProtection.DpapiDataProtectionProvider("ASP.NET IDENTITY");
                    _auserManager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(provider.Create("EmailConfirmation"))
                    {
                        TokenLifespan = TimeSpan.FromHours(24),
                    };
                    string code = _auserManager.GenerateEmailConfirmationToken(user.Id);
                    //var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    var callbackUrl = "http://healthkitchen.online/Account/ResetPassword?userId=" + user.Id + "&code=" + code;

                    _userManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                    result.Rflag = 1;
                    result.Message = "Forget email sent on your email [" + pEmail + "].";
                }
                else
                {
                    result.Rflag = 0;
                    result.Code = 1001;
                    result.Message = "";

                }
            }
            catch (Exception ex)
            {
                result.Rflag = 0;
                result.Code = 1001;
                result.Message = ex.ToString();
            }
            return result;
        }
        public IdentityUser LoginUser(string userName, string password)
        {
            IdentityUser user = _userManager.Find(userName, password);
            //ApplicationUser auser = _auserManager.FindById(user.Id);
            return user;
        }
        public ApplicationUser Login(string userName, string password)
        {
            //IdentityUser user = _userManager.Find(userName, password);
            ApplicationUser user = _auserManager.Find(userName, password);
            return user;
        }

        public async Task<IdentityUser> FindUser(string userName, string password)
        {
            IdentityUser user = await _userManager.FindAsync(userName, password);

            return user;
        }

       
      
      
      
      
     

      
        public async Task<IdentityResult> AddLoginAsync(string userId, UserLoginInfo login)
        {
            var result = await _userManager.AddLoginAsync(userId, login);

            return result;
        }

        public void Dispose()
        {
            _ctx.Dispose();
            _userManager.Dispose();

        }
    }



}
