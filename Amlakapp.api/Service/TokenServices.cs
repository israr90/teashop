﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Amlakapp.api.Models;
using Amlakapp.Core;

namespace Amlakapp.api
{
    public class TokenServices
    {
        public static object DbUtils { get; private set; }
        #region Public constructor.
        /// <summary>
        /// Public constructor.
        /// </summary>
        public TokenServices()
        {

        }
        #endregion


        #region Public member methods.

        /// <summary>
        ///  Function to generate unique token with expiry against the provided userId.
        ///  Also add a record in database for generated token.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static AppToken GenerateToken(string pUserId)
        {
            AppToken tokenModel = new AppToken();
            try
            {
                DeleteByUserId(pUserId);
                string token = Guid.NewGuid().ToString();
                DateTime issuedOn = DateTime.Now;
                DateTime expiredOn = DateTime.Now.AddMonths(1);

                string insertSQL = "INSERT INTO japitokens(UserId, IssuedOn, ExpiresOn, AuthToken)";
                insertSQL += " VALUES	('" + pUserId + "'";
                insertSQL += ", '" + issuedOn.ToString("yyyy/MM/dd HH:mm:ss") + "'";
                insertSQL += ", '" + expiredOn.ToString("yyyy/MM/dd HH:mm:ss") + "'";
                insertSQL += ", '" + token + "')";

                bool isTokenAdded = DBUtils.ExecuteSQL(insertSQL);
                if (isTokenAdded)
                {
                    tokenModel.UserId = pUserId;
                    tokenModel.IssuedOn = issuedOn;
                    tokenModel.ExpiresOn = expiredOn;
                    tokenModel.AuthToken = token;
                }
            }
            catch (Exception ex)
            {
                tokenModel.AuthToken += "Error";
            }
            return tokenModel;
        }

        public static AppToken GenerateOTPToken(string pUserId)
        {
            AppToken tokenModel = new AppToken();
            try
            {
                DeleteByUserId(pUserId);
                string token = Utils.getRandomDigit(6);
                DateTime issuedOn = DateTime.Now;
                DateTime expiredOn = DateTime.Now.AddSeconds(120);

                string insertSQL = "INSERT INTO japitokens(UserId, IssuedOn, ExpiresOn, AuthToken)";
                insertSQL += " VALUES	('" + pUserId + "'";
                insertSQL += ", '" + issuedOn.ToString("yyyy/MM/dd HH:mm:ss") + "'";
                insertSQL += ", '" + expiredOn.ToString("yyyy/MM/dd HH:mm:ss") + "'";
                insertSQL += ", '" + token + "')";

                bool isTokenAdded = DBUtils.ExecuteSQL(insertSQL);
                if (isTokenAdded)
                {
                    tokenModel.UserId = pUserId;
                    tokenModel.IssuedOn = issuedOn;
                    tokenModel.ExpiresOn = expiredOn;
                    tokenModel.AuthToken = token;
                }
            }
            catch (Exception ex)
            {
                tokenModel.AuthToken += "Error";
            }
            return tokenModel;
        }

        /// <summary>
        /// Method to get token object against expiry and existence in database.
        /// </summary>
        /// <param name="tokenId"></param>
        /// <returns></returns>
        public static AppToken GetToken(string pTokenId)
        {
            AppToken tokenModel = null;
            if (pTokenId != "")
            {
                string strSql = "SELECT * FROM japitokens  WHERE AuthToken = '" + pTokenId + "' and ExpiresOn > '" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "'";
                //string strSql = "SELECT * FROM japitokens  WHERE AuthToken = '" + pTokenId + "' and ExpiresOn > date('" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "')";
                DataTable dtToken = DBUtils.GetDataTable(strSql);
                if (dtToken != null)
                {
                    if (dtToken.Rows.Count > 0)
                    {
                        tokenModel = new AppToken();
                        tokenModel.UserId = dtToken.Rows[0]["UserId"].ToString();
                        tokenModel.IssuedOn = Convert.ToDateTime(dtToken.Rows[0]["IssuedOn"].ToString());
                        tokenModel.ExpiresOn = Convert.ToDateTime(dtToken.Rows[0]["ExpiresOn"].ToString());
                        tokenModel.AuthToken = pTokenId;
                        DateTime expiredOn = DateTime.Now.AddMonths(2);
                        //DateTime expiredOn = DateTime.Now.AddSeconds(9000);
                        string updateSQL = "update japitokens set ExpiresOn= '" + expiredOn.ToString("yyyy/MM/dd HH:mm:ss") + "' where AuthToken='" + pTokenId + "'";
                        DBUtils.ExecuteSQL(updateSQL);
                    }
                }
            }
            return tokenModel;
        }
        /// <summary>
        /// Method to validate token against expiry and existence in database.
        /// </summary>
        /// <param name="TokenId"></param>
        /// <returns></returns>
        public static bool ValidateToken(string pTokenId)
        {
            bool retVal = false;
            if (pTokenId != "")
            {
                string strTokenExpiresOn = DBUtils.executeSqlGetSingle("SELECT ExpiresOn FROM japitokens  WHERE AuthToken = '" + pTokenId + "' and ExpiresOn > '" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "' ");
                if (strTokenExpiresOn != "")
                {
                    DateTime tokenExpiresOn = Convert.ToDateTime(strTokenExpiresOn);
                    if (DateTime.Now > tokenExpiresOn)
                    {
                        DateTime expiredOn = DateTime.Now.AddSeconds(1500);
                        string updateSQL = "update japitokens set ExpiresOn= '" + expiredOn.ToString("yyyy/MM/dd HH:mm:ss") + "' where AuthToken='" + pTokenId + "'";
                        DBUtils.ExecuteSQL(updateSQL);
                        retVal = true;
                    }
                }
            }
            return retVal;
        }

        public static string ValidateOTPToken(string pTokenId)
        {
            string strUserId = "";
            if (pTokenId != "")
            {
                strUserId = DBUtils.executeSqlGetSingle("SELECT UserId FROM japitokens  WHERE AuthToken = '" + pTokenId + "' and ExpiresOn > '" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "' ");
                //if (strTokenExpiresOn != "")
                //{
                //    DateTime tokenExpiresOn = Convert.ToDateTime(strTokenExpiresOn);
                //    if (DateTime.Now > tokenExpiresOn)
                //    {
                //        //DateTime expiredOn = DateTime.Now.AddSeconds(1500);
                //        //string updateSQL = "update japitokens set ExpiresOn= '" + expiredOn.ToString("yyyy/MM/dd HH:mm:ss") + "' where AuthToken='" + pTokenId + "'";
                //        //DbUtils.ExecuteSQL(updateSQL);
                //        retVal = true;
                //    }
                //}
            }
            return strUserId;
        }

        /// <summary>
        /// Method to kill the provided token id.
        /// </summary>
        /// <param name="tokenId">true for successful delete</param>
        public static bool Kill(string pTokenId)
        {
            bool retVal = false;
            string deleteSQL = "Delete from japitokens where AuthToken='" + pTokenId + "'";
            retVal = DBUtils.ExecuteSQL(deleteSQL);
            return retVal;
        }

        /// <summary>
        /// Delete tokens for the specific deleted user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>true for successful delete</returns>
        public static bool DeleteByUserId(string pUserId)
        {
            bool retVal = false;
            string deleteSQL = "Delete from japitokens where UserId='" + pUserId + "'";
            retVal = DBUtils.ExecuteSQL(deleteSQL);
            return retVal;
        }

        #endregion
    }
}