using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.Mail;
using System.Net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using System.Net.Mail;
using System.Text.RegularExpressions;
using Amlakapp.Core;

namespace Amlakapp.api.Models
{
    
    public class AppUser
    {
        #region Public Id's
        public string UserID { get; set; }                             //-> agent_name
        public string FullName { get; set; }                    //-> agent_name
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Email { get; set; } //Required            //-> agent_email
        public string Phone { get; set; } //Required            //-> agent_phone
        public DateTime DOB { get; set; } //Required            //-> {Birth_day,Birth_Month,Birth_year}
        public string Gender { get; set; } //Required           //-> gender
        public string CountryName { get; set; } //Required   //-> agent_country
        public string CountryIsoCode { get; set; } //Required   //-> agent_country
        public string CountryCurrencyCode { get; set; }           //->CountryCurrencyCode    
        public string NationalityIsoCode { get; set; }          //-> nationality
        public string CountryOfBirthIsoCode { get; set; }       //-> country_code
        public string ZipCode { get; set; }
        private string PhoneCode { get; set; }
        private string StreetNo { get; set; }
        public string Address { get; set; } 	   
        //-> agent_address / agent_house_no
        public string City { get; set; }                        //-> agent_city
        public string PostalCode { get; set; }                  //-> agent_zip
        public int CustomerID { get; set; }                     //-> sender_list -> Id
        public int UserType { get; set; }                       //-> sender_list -> Id
        private int BirthDay { get; set; } //-> Birth_day
        private int BirthMonth { get; set; } //-> Birth_Month,
        private int BirthYear { get; set; } //-> Birth_year
    
        #endregion



        //public static PostUserResponse AppUserSignUp(PostUserRequest pPostUserRequest)
        //{
        //    PostUserResponse myPostUserResponse = new PostUserResponse();
        //    if (!string.IsNullOrEmpty(pPostUserRequest.Email) && pPostUserRequest.AuthType == 1)
        //    {
        //        #region userName
        //        bool isUserExistsByEmail = AppUser.UserExistsByEmail(pPostUserRequest.Username);
        //        if (!isUserExistsByEmail)
        //        {
        //            string tempPhoneNumber = " ";
        //           if (pPostUserRequest.PhoneNumber == null && pPostUserRequest.PhoneNumber == "")
        //           {
        //                tempPhoneNumber = "9999999999";
        //            }

        //            bool isInserted = InsertUser(pPostUserRequest.Username, pPostUserRequest.Password, tempPhoneNumber, pPostUserRequest.FirstName,pPostUserRequest.LastName);
        //            if (isInserted)
        //            {
                       
        //                AppUser mUser = new AppUser();
        //                mUser.FirstName = pPostUserRequest.FirstName;
        //                mUser.LastName = pPostUserRequest.LastName;
        //                mUser.Email = pPostUserRequest.Username;
        //                mUser.Username = pPostUserRequest.Username;
        //                myPostUserResponse.User = mUser;
        //                Utils.GetResult(ref myPostUserResponse.Result, "0");
        //                myPostUserResponse.Result.Description = "Registered successfully.";
        //            }
        //            else
        //            {
        //                Utils.GetResult(ref myPostUserResponse.Result, "290");
        //            }
        //        }
        //        else
        //        {
        //            Utils.GetResult(ref myPostUserResponse.Result, "203");
        //        }
        //        #endregion
        //    }
        //    else
        //    {
        //        Utils.GetResult(ref myPostUserResponse.Result, "100B");
        //    }
          
        //    return myPostUserResponse;
        //}

        #region get userProfile
        public static AppUser AppGetUserProfile(string pUserID)
        {
            AppUser myAppUser = new AppUser();
            if (!string.IsNullOrEmpty(pUserID))
            {
                var manager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var signInManager = HttpContext.Current.GetOwinContext().Get<ApplicationSignInManager>();
                var user = new ApplicationUser();
                user = manager.FindById(pUserID);
                if (user != null)
                {
                    myAppUser.UserID = user.Id;
                    myAppUser.FirstName = user.FirstName;
                    myAppUser.LastName = user.LastName;
                    myAppUser.FullName = user.FirstName + " " + user.LastName;
                    myAppUser.Email = user.Email;
                    myAppUser.Username = user.UserName;
                   // myAppUser.Phone = user.CellPhone;
                    myAppUser.UserType = user.UserType;
                    //myAppUser.DOB = Convert.ToDateTime(user.DateOfBirth);
                    //myAppUser.Address = user.Address1 + " " + user.Address2;
                    //myAppUser.Gender = user.Gender;
                    //myAppUser.CountryIsoCode = user.CountryISOCode;
                    myAppUser.City = user.City;
                    myAppUser.PostalCode = user.ZipCode;

                }
            }
            return myAppUser;
        }
        #endregion
        #region update profile
        public static UserProfileResponse UserUpdateProfile(UpdateProfileRequest pPostUpdateProfileRequest, string pUserId)
        {
            UserProfileResponse myPostUserResponse = new UserProfileResponse();
            AppUser myAppUser = new AppUser();
            myAppUser.FirstName = pPostUpdateProfileRequest.FirstName;
            myAppUser.LastName = pPostUpdateProfileRequest.LastName;
            myAppUser.Email = pPostUpdateProfileRequest.Email;
            myAppUser.Phone = pPostUpdateProfileRequest.Phone;
            myAppUser.DOB = Convert.ToDateTime(pPostUpdateProfileRequest.DOB);
            myAppUser.Address = pPostUpdateProfileRequest.Address;
            myAppUser.Gender = pPostUpdateProfileRequest.Gender;
            myAppUser.CountryIsoCode = pPostUpdateProfileRequest.CountryIsoCode;
            myAppUser.NationalityIsoCode = pPostUpdateProfileRequest.CountryOfBirthIsoCode;
            myAppUser.City = pPostUpdateProfileRequest.City;
            myAppUser.PostalCode = pPostUpdateProfileRequest.PostalCode;
            myAppUser.ZipCode = pPostUpdateProfileRequest.ZipCode;
            myAppUser.StreetNo = pPostUpdateProfileRequest.StreetNo;
            myAppUser.PhoneCode = pPostUpdateProfileRequest.PhoneCode;
            string isUpdated = UpdateUser(myAppUser, pUserId);
            if (isUpdated != "")
            {
                Utils.GetResult(ref myPostUserResponse.Result, "0");
                myPostUserResponse.Result.Description = isUpdated;
                myPostUserResponse.Data = myAppUser;
            }
            else
            {
                Utils.GetResult(ref myPostUserResponse.Result, "290");
            }
            return myPostUserResponse;
        }
        #endregion
        public static ForgetPasswordResponse ForgetPassword(string pEmail)
        {
            ForgetPasswordResponse myyPostUserForgetPasswordResponse = new ForgetPasswordResponse();
            try
            {
                var manager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
                ApplicationUser user = manager.FindByEmail(pEmail);
                //if (user == null || !manager.IsEmailConfirmed(user.Id))
                    if (user == null)
                {
                    Utils.GetResult(ref myyPostUserForgetPasswordResponse.Result, "204");
                    myyPostUserForgetPasswordResponse.Data = "The user either does not exist or is not confirmed.";
                }
                else
                {
                    string code = manager.GeneratePasswordResetToken(user.Id);
                    //string PindCode  = DbUtils.executeSqlGetSingle("select PinCode from [User] where Email = '" + pEmail + "'");
                    var fromAddress = new MailAddress("info.onecarapp@gmail.com", "info_onecar");
                    var toAddress = new MailAddress(pEmail);
                    const string fromPassword = "Pak_9381";
                    const string subject = "Forgot password";
                     string body = "PIN CODE:"+ code;

                    var smtp = new SmtpClient
                    {
                        Host = "smtp.gmail.com",
                        Port = 587,
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        Credentials = new NetworkCredential(fromAddress.Address, fromPassword),
                        Timeout = 20000
                    };
                    using (var message = new System.Net.Mail.MailMessage(fromAddress, toAddress)
                    {
                        Subject = subject,
                        Body = body
                    })
                    {
                        smtp.Send(message);
                    }
                    myyPostUserForgetPasswordResponse.Data += "Dear Customer an email is sent on your email ID  " + pEmail + "<br/>Please follow the instructions in email to reset your password";
                    Utils.GetResult(ref myyPostUserForgetPasswordResponse.Result, "0");
                }
            }
            catch(Exception xp)
            {
                Utils.GetResult(ref myyPostUserForgetPasswordResponse.Result, "291");
            }
            return myyPostUserForgetPasswordResponse;
        }
        private static string UpdateUser(AppUser myAppUser, string pUserID)
        {
          bool retVal = false;
            string retVal2 = "";
            try
            {
                if (myAppUser != null)
                {
                    string updateSQL = "Update [user] set ";
                    if (!string.IsNullOrEmpty(myAppUser.FirstName))
                        updateSQL += " FirstName ='" + myAppUser.FirstName + "',";
                    if (!string.IsNullOrEmpty(myAppUser.LastName))
                        updateSQL += " LastName='" + myAppUser.LastName + "',";
                    if (!string.IsNullOrEmpty(myAppUser.CountryIsoCode))
                        updateSQL += " CountryISOCode='" + myAppUser.CountryIsoCode + "',";
                    if (!string.IsNullOrEmpty(myAppUser.Phone))
                        updateSQL += " CellPhone='" + myAppUser.Phone + "',";
                    //if (!string.IsNullOrEmpty(myAppUser.DOB.ToString()))
                    //{
                    //    if (myAppUser.DOB.ToString() != "1/1/0001 12:00:00 AM")
                    //    {
                    //        updateSQL += " DOB='" + myAppUser.DOB + "',";
                    //    }
                    //}
                    if (!string.IsNullOrEmpty(myAppUser.Email))
                        updateSQL += " Email='" + myAppUser.Email + "',";
                    if (!string.IsNullOrEmpty(myAppUser.Gender))
                        updateSQL += " Gender='" + myAppUser.Gender + "',";
                    if (!string.IsNullOrEmpty(myAppUser.PostalCode))
                        updateSQL += " ZipCode='" + myAppUser.PostalCode + "',";
                    if (!string.IsNullOrEmpty(myAppUser.City))
                        updateSQL += " City='" + myAppUser.City + "',";
                    if (!string.IsNullOrEmpty(myAppUser.ZipCode))
                        updateSQL += " ZipCode='" + myAppUser.ZipCode + "',";
                    if (!string.IsNullOrEmpty(myAppUser.StreetNo))
                        updateSQL += " StreetNo='" + myAppUser.StreetNo + "',";
                    if (!string.IsNullOrEmpty(myAppUser.PhoneCode))
                        updateSQL += " PhoneCode='" + myAppUser.PhoneCode + "',";
                    if (!string.IsNullOrEmpty(myAppUser.Address))
                        updateSQL += " Address1='" + myAppUser.Address + "',";

                    updateSQL += " Address2=''";
                    updateSQL += " where UserID='" + pUserID + "'";
                    retVal2 = updateSQL;
                    bool isUserUpdated = DBUtils.ExecuteSQL(updateSQL);
                    if (isUserUpdated)
                    {
                        retVal2 += "(successfull)";
                        retVal = true;
                    }
                }
            }
            catch
            {
                retVal = false;
            }
            return retVal2;
        }
        public static bool InsertUser(string pUserName, string pPassword,string pPhoneNumber, string FirstName,string LastName)
        {
            bool retVal = false;
            try
            {
                if (!string.IsNullOrEmpty(pUserName) && !string.IsNullOrEmpty(pPassword))
                {
                    string pinCode = Utils.getRandomDigit(6);
                    var manager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
                    var signInManager = HttpContext.Current.GetOwinContext().Get<ApplicationSignInManager>();
                    var user = new ApplicationUser()
                    {
                        FirstName=FirstName,
                        LastName = LastName,
                        UserName = pUserName,
                        Email = pUserName,
                        UserType = (byte)25,
                       // CellPhone= pPhoneNumber,
                       // PinCode = pinCode,
                     //   DateAdded = DateTime.UtcNow,
                        Status = true,
                        EmailConfirmed = true,
                        PhoneNumberConfirmed = true
                    };

                    IdentityResult result = manager.Create(user, pPassword);
                    if (result.Succeeded)
                    {
                        manager.AddToRole(user.Id, "Agent");
                        retVal = true;
                    }
                }
                else
                {
                    retVal = false;
                }
            }
            catch(Exception xp)
            {
                retVal = false;
            }
            return retVal;
        }
        public static bool UserExistsByEmail(string pEmail)
        {
            bool retVal = false;
            if (pEmail.Trim() != "")
            {
                string myEmail = DBUtils.executeSqlGetSingle("select Email from [User] where Email = '" + pEmail + "'");
                if (!string.IsNullOrEmpty(myEmail))
                {
                    retVal = true;
                }
            }
            return retVal;
        }
       
        public static bool ValidateUser(string myUserName, string myPassword)
        {
            bool RetVal = false;
            try
            {
                if (myUserName.Trim() != "" && myPassword.Trim() != "")
                {
                    var manager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
                    var signInManager = HttpContext.Current.GetOwinContext().Get<ApplicationSignInManager>();
                    var user = new ApplicationUser();
                    user = manager.FindByEmail(myUserName);
                    if (user != null)
                    {
                        RetVal = manager.CheckPassword(user, myPassword);
                    }
                    else if (user != null)
                    {
                        //string URI = "http://www.sendtopup.co.uk/JNChangeNConfirm.aspx?Req=VE&Email=" + myUserName;
                        //string myParameters = "";
                        //using (WebClient wc = new WebClient())
                        //{
                        //    wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                        //    string HtmlResult = wc.UploadString(URI, myParameters);
                        //}
                    }
                }
            }
            catch(Exception xp)
            {
                RetVal = false;
            }
            return RetVal;
        }      

        public static bool IsPositive(decimal number)
        {
            return number > 0;
        }
        public static bool IsNegative(decimal number)
        {
            return number < 0;
        }
      
      
      
    }
}
