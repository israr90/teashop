﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amlakapp.api.Models
{
    public class RegisterViewModel
    {
       

        //[Required(ErrorMessage = "Please enter user first name")]
        public string FirstName { get; set; }

        //[Required(ErrorMessage = "Please enter user last name")]
        public string LastName { get; set; }

        public string OfficePhone { get; set; }
        //[Required(ErrorMessage = "Please enter user cell phone")]
        public string CellPhone { get; set; }

        public string City { get; set; }
        public string UserType { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string FaxNumber { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string UserName { get; set; }

    //  public DateTime DateOfBirth { get; set; }

        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }
        public string ZipCode { get; set; }
        public string CountryISOCode { get; set; }
        public string HomePhone { get; set; }
    }

    public class ChangePasswordViewModel
    {

        public string Status { get; set; }
        public string ExPassword { get; set; }
        public bool IsSendEmail { get; set; }
        public string UserID { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Akey { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "NewPassword")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("NewPassword", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmNewPassword { get; set; }

      

        

        
      
       
    }

    #region Register User Request/Response
    /// <summary>
    /// Register Company Request object
    /// </summary>
    public class RegisterUserRequest
    {       
        /// <summary>
        /// User data object
        /// </summary>
       [Required]
        public RegisterViewModel registerViewModel { get; set; }
    }
    /// <summary>
    /// Register Customer Response object
    /// </summary>
    public class RegisterUserResponse
    {
        /// <summary>
        /// result objects
        /// 
        /// </summary>
        public AppUser Data { get; set; }
        public Result Result = new Result();
        public string AuthToken { get; set; }
        
    }

    #endregion
}
