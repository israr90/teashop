﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Amlakapp.api.Models
{
    #region Request/Response
    public class PostUserRequest
    {
        public string AuthKey { get; set; }
        public string Password { get; set; }
        //public string Username { get; set; }
        public string Email { get; set; }
        //public string PhoneNumber { get; set; }
        //public string FirstName { get; set; }
        //public string LastName { get; set; }
        public int AuthType { get; set; } //1=Username/pass, 2=PhoneNumber, 3=FaceBook, 4=Google
    }
    public class PostSigninUserRequest
    {
        public string AuthKey { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string PhoneNumber { get; set; }
        public string PhoneAuthKey { get; set; }
        public string FacebookAuthKey { get; set; }
        public string GoogleAuthKey { get; set; }
        public int AuthType { get; set; } //1=Username/pass, 2=PhoneNumber, 3=FaceBook, 4=Google
    }
    public class PostUserResponse
    {
        public AppUser data { get; set; }
        public Result result = new Result();
        public string AuthToken { get; set; }
    }
    public class UserProfileRequest
    {
        public string AuthKey { get; set; }
        public string AuthToken { get; set; }
    }
    public class UserProfileResponse
    {
        public Result Result = new Result();
        public AppUser Data { get; set; }
    }
    public class UpdateProfileRequest
    {
        public string UserID { get; set; }
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public string CountryName { get; set; }
        public string CountryIsoCode { get; set; }
        public string CountryCurrencyCode { get; set; }
        public string NationalityIsoCode { get; set; }
        public string CountryOfBirthIsoCode { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string PhoneCode { get; set; }
        public string StreetNo { get; set; }
        public string PostalCode { get; set; }
        public int CustomerID { get; set; }
        public int UserType { get; set; }
        private int BirthDay { get; set; }
        private int BirthMonth { get; set; }
        private int BirthYear { get; set; }
        public string AuthKey { get; set; }
        public string AuthToken { get; set; }
    }
    public class ForgetPasswordRequest
    {
        public string AuthKey { get; set; }
        public string Email { get; set; }
    }
    public class ForgetPasswordResponse
    {
        public string Data { get; set; }
        //public string Message { get; set; }
        public Result Result = new Result();
    }
    public class PostVerifyOTPRequest
    {
        public string OTP { get; set; }
        public string PhoneNumber { get; set; }
    }
    public class PostVerifyOTPResponse
    {
        public string AuthKey { get; set; }
        public Result Result = new Result();
    }

    public class PostResetPasswordRequest
    {
        public string AuthKey { get; set; }
        public int AuthType { get; set; }
        public string Email { get; set; }
        public string PinCode { get; set; }
        public string NewPassword { get; set; }
    }
    public class ResetPasswordResponse
    {
        public string AuthKey { get; set; }
        public Result Result = new Result();
        public string Data { get; set; }
        //public string Email { get; set; }

    }
    #endregion
}