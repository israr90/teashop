﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Amlakapp.api.Models
{    public class Result
    {
        public int Rflag;
        public int Code;
        public string Message;
        public string Description;
    }
}