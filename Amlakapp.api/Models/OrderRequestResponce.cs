using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Amlakapp.Entities;
namespace Amlakapp.api.Models
{
    public class OrderListRequest
    {
        public string AuthKey { get; set; }
        public string AuthToken { get; set; }
    }
    public class OrderListResponce
    {
        public Result Result = new Result();
        public List<Order> Data = new List<Order>();
    }

    public class NewOrderRequest
    {
        public string AuthKey { get; set; }
        public string AuthToken { get; set; }
        public List<Product> Products { get; set; }
        public string DeliveryAddress { get; set; }
        public double TotalPrice { get; set; }
        public string OrderBy { get; set; }
       // public DateTime OrderDate { get; set; }
    }

    public class NewOrderResponse
    {
        public Result Result = new Result();
        public Order Data = new Order();
    }
    public class OrderService
    {
        internal static NewOrderResponse NewOrder(NewOrderRequest porderRequest)
        {
            NewOrderResponse obj = new NewOrderResponse();
            try
            {
                db_amlakEntities db = new db_amlakEntities();
                Order od = new Order();
                od.OrderBy = porderRequest.OrderBy;
                od.OrderDate = DateTime.Now;
                od.OrderStatus = "Pending";
                od.DeliveryAddress = porderRequest.DeliveryAddress;
                od.TotalPrice = porderRequest.TotalPrice ;
                od.UserName = db.Users.Find(porderRequest.OrderBy).UserName;
                db.Orders.Add(od);
                db.SaveChanges();
                if (od.OrderId > 0)
                {
                    foreach(var item in porderRequest.Products)
                    {
                        OrderProduct op = new OrderProduct();
                        op.OrderBy = porderRequest.OrderBy;
                        op.OrderId =od.OrderId;
                        op.Quantity = item.ProductQuantity;
                        op.Price = item.ProductPrice;
                        op.ProductId = item.ProductID;
                        db.OrderProducts.Add(op);
                        db.SaveChanges();
                    }
                    obj.Result.Code = 0;
                    obj.Data.DeliveryAddress = porderRequest.DeliveryAddress;
                    obj.Data.UserName = db.Users.Find(porderRequest.OrderBy).UserName;
                    obj.Data.OrderBy = od.OrderBy;
                    obj.Data.OrderDate = od.OrderDate;
                    obj.Data.OrderStatus = "Pending";
                    obj.Data.UserName = db.Users.Find(porderRequest.OrderBy).UserName;
                    obj.Data.TotalPrice = od.TotalPrice;
                }

            }
            catch(Exception exp)
            {
                obj.Result.Code = 003;
               
            }
            return obj;
        }
    }


}
