using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Amlakapp.Entities;

namespace Amlakapp.api.Models
{
    public class ProductListRequest
    {
        public string AuthKey { get; set; }
        public string AuthToken { get; set; }
    }
    public class ProductListResponce
    {
        public Result Result = new Result();
        public List<Product> Data = new List<Product>(); 
    }
}
