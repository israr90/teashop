﻿using Amlakapp.api.Models;
using Amlakapp.Core;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace Amlakapp.api
{
    public static class Utils
    {
        private const string ENCRYPTION_KEY = "#i%R&e^M~*";
        private static byte[] _keyBytes = { 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18 };

        #region Validate Models
        public static string AuthKey = ConfigurationManager.AppSettings["AuthKey"].ToString();
        public static bool ValidateAppUser(PostUserRequest pPostUserRequest)
        {
            bool RetVal = false;
            if (pPostUserRequest != null)
            {
                //if (!string.IsNullOrEmpty(pPostUserRequest.Username) && !string.IsNullOrEmpty(pPostUserRequest.Password) && !string.IsNullOrEmpty(pPostUserRequest.PhoneNumber))                
                if (!string.IsNullOrEmpty(pPostUserRequest.Email))
                {
                    if (!string.IsNullOrEmpty(pPostUserRequest.Password))
                    {
                        RetVal = true;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(pPostUserRequest.Password))
                    {
                        RetVal = true;
                    }
                }

            }
            return RetVal;
        }


        public static Result ValidateUserRequest(RegisterViewModel pUser)
        {
            Result myResult = new Result();
            myResult.Rflag = 1;
            myResult.Code = 0;
            myResult.Message = "Completed";

            #region Customer Info
            if (pUser != null)
            {
                //if (string.IsNullOrEmpty(pUser.FirstName))
                //{
                //    myResult.Rflag = 0;
                //    myResult.Code = 3000;
                //    return myResult;
                //}
                //if (string.IsNullOrEmpty(pUser.LastName))
                //{
                //    myResult.Rflag = 0;
                //    myResult.Code = 3001;
                //    return myResult;
                //}
                if (string.IsNullOrEmpty(pUser.Email))
                {
                    myResult.Rflag = 0;
                    myResult.Code = 3002;
                    return myResult;
                }
                if (string.IsNullOrEmpty(pUser.Password))
                {
                    myResult.Rflag = 0;
                    myResult.Code = 3001;
                    return myResult;
                }
            }
            else
            {
                myResult.Rflag = 0;
                myResult.Code = 1004;
                return myResult;
            }
            return myResult;
            #endregion
        }


        public static bool ValidateAppUserProfile(UpdateProfileRequest pPostUpdateProfileRequest)
        {
            bool RetVal = false;
            if (pPostUpdateProfileRequest != null)
            {
                if (pPostUpdateProfileRequest != null)
                {
                    RetVal = true;
                }
            }
            return RetVal;
        }

        #endregion

        #region ValidateAuthKey

        public static bool ValidateAuthKey(dynamic pPostRequest)
        {
            string myAuthKey = AuthKey;
            bool RetVal = false;
            if (pPostRequest != null && pPostRequest.AuthKey != null)
            {
                if (!string.IsNullOrEmpty(pPostRequest.AuthKey))
                {
                    if (pPostRequest.AuthKey.Trim() != "")
                    {
                        if (pPostRequest.AuthKey.Trim() == myAuthKey.Trim())
                        {
                            RetVal = true;
                        }
                    }
                }
                if (pPostRequest.AuthToken == null)
                {
                    RetVal = false;
                }
            }
            return RetVal;
        }
        public static bool ValidateAuthKeyWithoutToken(dynamic pPostRequest)
        {
            string myAuthKey = AuthKey;
            bool RetVal = false;
            if (pPostRequest != null && pPostRequest.AuthKey != null)
            {
                if (!string.IsNullOrEmpty(pPostRequest.AuthKey))
                {
                    if (pPostRequest.AuthKey.Trim() != "")
                    {
                        if (pPostRequest.AuthKey.Trim() == myAuthKey.Trim())
                        {
                            RetVal = true;
                        }
                    }
                }
            }
            return RetVal;
        }
        public static bool ValidateStringAuthKey(string pAuthKey)
        {
            string myAuthKey = AuthKey;
            bool RetVal = false;
            if (pAuthKey != "")
            {
                if (pAuthKey.Trim() == myAuthKey.Trim())
                {
                    RetVal = true;
                }
            }
            return RetVal;
        }

        #endregion



        #region GetMethods
        public static string GetUserFullNameByUserID(string pUserID)
        {
            string retVal = DBUtils.executeSqlGetSingle("select FirstName + ' ' + LastName from [User] where UserID='" + pUserID + "'");
            return retVal;
        }
       
      
       
        public static string getRandomDigit(int codeCount)
        {
            string allChar = "1,2,3,4,5,6,7,8,9,0";
            string[] allCharArray = allChar.Split(',');
            string randomCode = "";
            int temp = -1;

            Random rand = new Random();
            for (int i = 0; i < codeCount; i++)
            {
                if (temp != -1)
                {
                    rand = new Random(i * temp * ((int)DateTime.Now.Ticks));
                }
                int t = rand.Next(9);
                if (temp != -1 && temp == t)
                {
                    return getRandomDigit(codeCount);
                }
                temp = t;
                randomCode += allCharArray[t];
            }
            return randomCode;
        }
        public static string getRandomString(int length)
        {
            string allowedLetterChars = "abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ";
            string allowedNumberChars = "23456789";
            char[] chars = new char[length];
            Random rd = new Random();
            bool useLetter = true;
            for (int i = 0; i < length; i++)
            {
                if (useLetter)
                {
                    chars[i] = allowedLetterChars[rd.Next(0, allowedLetterChars.Length)];
                    useLetter = false;
                }
                else
                {
                    chars[i] = allowedNumberChars[rd.Next(0, allowedNumberChars.Length)];
                    useLetter = true;
                }
            }
            return new string(chars);
        }
        public static string SaveBase64AsImage(string SavePath, string base64String)
        {
            string fullOutputPath = "";
            try
            {
                //fullOutputPath = SavePath + "/" + Utils.getRandomString(15);
                fullOutputPath = Utils.getRandomString(10) + Utils.getRandomDigit(10);

                byte[] bytes = Convert.FromBase64String(base64String);
                System.Drawing.Image image;
                using (MemoryStream ms = new MemoryStream(bytes))
                {
                    image = System.Drawing.Image.FromStream(ms);
                }
                string myType = base64String.Substring(0, 1);
                if (myType == "/")
                {
                    //return "image/jpeg";
                    fullOutputPath += ".jpg";
                    using (Bitmap tempImage = new Bitmap(image))
                    {
                        tempImage.Save(SavePath + "/" + fullOutputPath, System.Drawing.Imaging.ImageFormat.Jpeg);
                    }

                    //image.Save(SavePath + "/" + fullOutputPath, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
                else if (myType == "R")
                {
                    //return "image/gif";
                    fullOutputPath += ".gif";
                    image.Save(SavePath + "/" + fullOutputPath, System.Drawing.Imaging.ImageFormat.Gif);
                }
                else if (myType == "i")
                {
                    //return "image/png";
                    fullOutputPath += ".png";
                    image.Save(SavePath + "/" + fullOutputPath, System.Drawing.Imaging.ImageFormat.Png);
                }
                else
                {
                    fullOutputPath += ".png";
                    image.Save(SavePath + "/" + fullOutputPath, System.Drawing.Imaging.ImageFormat.Png);
                }

                //ret = "saved";
            }
            catch (Exception ex)
            {
                fullOutputPath = "";
                //ret = "";
            }
            return fullOutputPath;
        }
        public static string SerializeObjectToXML(object item)
        {
            try
            {
                string xmlText;
                //Get the type of the object
                Type objectType = item.GetType();
                //create serializer object based on the object type
                XmlSerializer xmlSerializer = new XmlSerializer(objectType);
                //Create a memory stream handle the data
                MemoryStream memoryStream = new MemoryStream();
                //Create an XML Text writer to serialize data to
                using (XmlTextWriter xmlTextWriter =
                    new XmlTextWriter(memoryStream, Encoding.UTF8) { Formatting = Formatting.Indented })
                {
                    //convert the object to xml data
                    xmlSerializer.Serialize(xmlTextWriter, item);
                    //Get reference to memory stream
                    memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                    //Convert memory byte array into xml text
                    xmlText = new UTF8Encoding().GetString(memoryStream.ToArray());
                    //clean up memory stream
                    memoryStream.Dispose();
                    return xmlText;
                }
            }
            catch (Exception e)
            {
                //There are a number of reasons why this function may fail
                //usually because some of the data on the class cannot
                //be serialized.
                System.Diagnostics.Debug.Write(e.ToString());
                return null;
            }
        }

        #endregion

        #region  Error Code/Result
        public static void GetResult(ref Result myResult, string Code)
        {
            if (Code == "0")// if successfully done
            {
                myResult.Rflag = 1;
                myResult.Code = 0;
                myResult.Message = "You have completed successfully";
            }
            if (Code == "2")
            {
                myResult.Rflag = 1;
                myResult.Code = 0;
                myResult.Message = "Invalid Pincode";
            }
            if (Code == "2A")
            {
                myResult.Rflag = 1;
                myResult.Code = 0;
                myResult.Message = "Please provide pincode.";
            }
            if (Code == "2B")
            {
                myResult.Rflag = 1;
                myResult.Code = 0;
                myResult.Message = "Please provide new Password.";
            }
            else if (Code == "100")// authkey
            {
                myResult.Rflag = 0;
                myResult.Code = 100;
                myResult.Message = "Invalid AuthKey Provided.";
            }
            else if (Code == "100A")// authkey & authtoken
            {
                myResult.Rflag = 0;
                myResult.Code = 100;
                myResult.Message = "Invalid AuthKey or AuthToken Provided.";
            }
            else if (Code == "100B")// authkey & authtoken
            {
                myResult.Rflag = 0;
                myResult.Code = 100;
                myResult.Message = "Invalid AuthType Provided.";
            }
           
            else if (Code == "101")//authtoken
            {
                myResult.Rflag = 0;
                myResult.Code = 101;
                myResult.Message = "Invalid AuthToken Provided.";
            }
            else if (Code == "102") //authtoken
            {
                myResult.Rflag = 0;
                myResult.Code = 102;
                myResult.Message = "Invalid User Account. AuthToken not generated.";
            }
            else if (Code == "103") //authtoken
            {
                myResult.Rflag = 0;
                myResult.Code = 103;
                myResult.Message = "System error. AuthToken not generated.";
            }
          
            else if (Code == "105") //Invalid PhoneNumber and/or countryid 
            {
                myResult.Rflag = 0;
                myResult.Code = 105;
                myResult.Message = "Invalid or empty PhoneNumber.";
            }
            else if (Code == "106") //Invalid PhoneNumber and/or countryid 
            {
                myResult.Rflag = 0;
                myResult.Code = 106;
                myResult.Message = "Invalid or empty OTP.";
            }
            else if (Code == "107") //Invalid or empty PhoneAuthKey 
            {
                myResult.Rflag = 0;
                myResult.Code = 107;
                myResult.Message = "Invalid or empty PhoneAuthKey.";
            }
            else if (Code == "108") //
            {
                myResult.Rflag = 0;
                myResult.Code = 107;
                myResult.Message = "Invalid or empty Type.";
            }
            else if (Code == "201")//user
            {
                myResult.Rflag = 0;
                myResult.Code = 201;
                myResult.Message = "Invalid AppUser  Object. Please provide complete information.";
            }
            else if (Code == "202")
            {
                myResult.Rflag = 0;
                myResult.Code = 202;
                myResult.Message = "Invalid Email/Password.";
            }
            else if (Code == "203")
            {
                myResult.Rflag = 0;
                myResult.Code = 203;
                myResult.Message = "Email already used for registration. Please provide alternate email";
            }
            else if (Code == "204")
            {
                myResult.Rflag = 0;
                myResult.Code = 204;
                myResult.Message = "Invalid email provided.";
            }
            else if (Code == "205")
            {
                myResult.Rflag = 0;
                myResult.Code = 205;
                myResult.Message = "Invalid token and new password provided.";
            }
            else if (Code == "206")
            {
                myResult.Rflag = 0;
                myResult.Code = 206;
                myResult.Message = "Please provide year information.";
            }
            else if (Code == "207")
            {
                myResult.Rflag = 0;
                myResult.Code = 207;
                myResult.Message = "Phone Number already used for registration. Please provide alternate Phone Number.";
            }
            else if (Code == "208")
            {
                myResult.Rflag = 0;
                myResult.Code = 208;
                myResult.Message = "Invalid Username or Password.";
            }
            else if (Code == "290")
            {
                myResult.Rflag = 0;
                myResult.Code = 290;
                myResult.Message = "Generel Exception.Error 290";
            }
            else if (Code == "291")
            {
                myResult.Rflag = 0;
                myResult.Code = 291;
                myResult.Message = "he SMTP server requires a secure connection or the client was not authenticated. The server response was: 5.7.0 Authentication Required.";
            }


            else if (Code == "305")
            {
                myResult.Rflag = 0;
                myResult.Code = 305;
                myResult.Message = "Please Provide UserType.";
            }
            else if (Code == "306")
            {
                myResult.Rflag = 0;
                myResult.Code = 306;
                myResult.Message = "Please Provide CurrencyISOCode.";
            }
            else if (Code == "307")
            {
                myResult.Rflag = 0;
                myResult.Code = 307;
                myResult.Message = "Undefinned";
            }
            else if (Code == "401")// Deposits
            {
                myResult.Rflag = 0;
                myResult.Code = 401;
                myResult.Message = "No Record Found.";
            }
         


            else if (Code == "10100") //sender error codes area started
            {
                myResult.Rflag = 0;
                myResult.Code = 10100;
                myResult.Message = "Please provide  first name.";
            }
            else if (Code == "10101")
            {
                myResult.Rflag = 0;
                myResult.Code = 10101;
                myResult.Message = "Please provide last name.";
            }
            else if (Code == "10102")
            {
                myResult.Rflag = 0;
                myResult.Code = 10102;
                myResult.Message = "Please provide sender telephone no.";
            }
            else if (Code == "10103")
            {
                myResult.Rflag = 0;
                myResult.Code = 10103;
                myResult.Message = "Please provide sender address.";
            }
            else if (Code == "10104")
            {
                myResult.Rflag = 0;
                myResult.Code = 10104;
                myResult.Message = "Please provide sender nationality.";
            }
            else if (Code == "10105")
            {
                myResult.Rflag = 0;
                myResult.Code = 10105;
                myResult.Message = "Please provide sender city.";
            }
            else if (Code == "10106")
            {
                myResult.Rflag = 0;
                myResult.Code = 10106;
                myResult.Message = "Please provide sender issue date.";
            }
            else if (Code == "10107")
            {
                myResult.Rflag = 0;
                myResult.Code = 10107;
                myResult.Message = "Please provide sender expiry date.";
            }
            else if (Code == "10108")
            {
                myResult.Rflag = 0;
                myResult.Code = 10108;
                myResult.Message = "Please provide Agent  id number.";
            }
            else if (Code == "101090")
            {
                myResult.Rflag = 0;
                myResult.Code = 101090;
                myResult.Message = "Please provide valid sender id docs.";
            }
            else if (Code == "10109")
            {
                myResult.Rflag = 0;
                myResult.Code = 10109;
                myResult.Message = "Please provide valid sender id type.";
            }
            else if (Code == "10114")
            {
                myResult.Rflag = 0;
                myResult.Code = 10114;
                myResult.Message = "Please provide sender Date Of Birth.";
            }
            else if (Code == "10115")
            {
                myResult.Rflag = 0;
                myResult.Code = 10115;
                myResult.Message = "Please provide valid sender nationality.";
            }
            else if (Code == "10110")
            {
                myResult.Rflag = 0;
                myResult.Code = 10110;
                myResult.Message = "Please provide country code.";
            }
           
            else if (Code == "10112")
            {
                myResult.Rflag = 0;
                myResult.Code = 10112;
                myResult.Message = "Please provide sender id number.";
            }

            //user information error
            else if (Code == "3000")
            {
                myResult.Rflag = 0;
                myResult.Code = 3000;
                myResult.Message = "Please provide user first name.";
            }
            else if (Code == "3001")
            {
                myResult.Rflag = 0;
                myResult.Code = 3001;
                myResult.Message = "Please provide password.";
            }
            else if (Code == "3002")
            {
                myResult.Rflag = 0;
                myResult.Code = 3002;
                myResult.Message = "Please provide user email address.";
            }
            else if (Code == "3003")
            {
                myResult.Rflag = 0;
                myResult.Code = 3002;
                myResult.Message = "Email address not registered.";
            }
            else if (Code == "3004")
            {
                myResult.Rflag = 0;
                myResult.Code = 3004;
                myResult.Message = "An error occured while getting images";
            }
            else if (Code == "3005")
            {
                myResult.Rflag = 0;
                myResult.Code = 3005;
                myResult.Message = "An error occured in Add Favorite";
            }
            else if (Code == "3006")
            {
                myResult.Rflag = 0;
                myResult.Code = 3006;
                myResult.Message = "Please provide HomeFactID";
            }

            else if (Code == "3007")
            {
                myResult.Rflag = 0;
                myResult.Code = 3006;
                myResult.Message = "Thid ads is already in Favorite list";
            }

        }

        public static string GetError(string Code)
        {
            string ErrorMessege = "";
            if (Code == "0")
            {
                ErrorMessege = "Successfully done";
            }
            else if (Code == "10000")
            {
                ErrorMessege = "Authentication Failure.";
            }
            else if (Code == "10003")
            {
                ErrorMessege = "Invalid Date Format";
            }
           
            else if (Code == "10015")
            {
                ErrorMessege = "Error in updating agent owning balance.";
            }
          
            else if (Code == "10100") //sender error codes area started
            {
                ErrorMessege = "Please provide sender first name.";
            }
            else if (Code == "10101")
            {
                ErrorMessege = "Please provide sender last name.";
            }
            else if (Code == "10102")
            {
                ErrorMessege = "Please provide sender telephone no.";
            }
            else if (Code == "10103")
            {
                ErrorMessege = "Please provide sender address.";
            }
            else if (Code == "10104")
            {
                ErrorMessege = "Please provide sender nationality.";
            }
            else if (Code == "10105")
            {
                ErrorMessege = "Please provide sender city.";
            }
            else if (Code == "10106")
            {
                ErrorMessege = "Please provide sender issue date.";
            }
            else if (Code == "10107")
            {
                ErrorMessege = "Please provide sender expiry date.";
            }
            else if (Code == "10108")
            {
                ErrorMessege = "Please provide Agent  id number.";
            }
            else if (Code == "10109")
            {
                ErrorMessege = "Please provide sender id type.";
            }
            else if (Code == "10114")
            {
                ErrorMessege = "Please provide sender Date Of Birth.";
            }
            else if (Code == "10115")
            {
                ErrorMessege = "Please provide valid sender nationality.";
            }
       

            return ErrorMessege;
        }
        #endregion

        #region Email
        public static string SendEmail(string AddressTo, string AddressFrom, string Subject, string Body)
        {
            string myVal = "";
            try
            {
                myVal += "1a";
                string smtp_host = "localhost";
                smtp_host = "mail.jahantopup.com";
                myVal += "smtp_host=" + smtp_host;
                if (AddressFrom != "" && AddressTo != "" && Subject != "" && Body != "" && smtp_host != "")
                {
                    char[] separator = new char[] { ',' };
                    string[] exp_list = AddressTo.Split(separator);
                    for (int i = 0; i < exp_list.Length; i++)
                    {
                        string email_to = exp_list[i].ToString().Trim();
                        bool is_valid = Utils.IsValidEmail(AddressFrom);
                        bool is_valid2 = Utils.IsValidEmail(email_to);
                        if (is_valid == true && is_valid2 == true)
                        {
                            MailMessage msgMail = new MailMessage(AddressFrom, email_to);

                            myVal += "-AddressFrom=" + AddressFrom;
                            myVal += "-email_to=" + email_to;

                            msgMail.Subject = Subject;
                            Body = HttpUtility.HtmlDecode(Body);
                            msgMail.Body = Body;

                            msgMail.BodyEncoding = System.Text.Encoding.UTF8;
                            msgMail.IsBodyHtml = true;

                            myVal += "-Body=" + Body;
                            myVal += "-Subject=" + Subject;
                            SmtpClient myClient = new SmtpClient();
                            myClient.Host = smtp_host;
                            myVal += "Host" + smtp_host;
                            myClient.Port = 26;
                            myVal += "Port" + 26;
                            myClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                            myClient.UseDefaultCredentials = false;
                            //if (!string.IsNullOrEmpty(SysPrefs.SMTPUseSSL))
                            //{
                            //    if (SysPrefs.SMTPUseSSL.ToString().Trim() == "1")
                            //    {
                            //        myVal += "SMTPUseSSL" + 0;
                            //myClient.EnableSsl = true;
                            //    }
                            //}

                            myClient.Credentials = new System.Net.NetworkCredential("auto@jahantopup.com", "Abc123*");
                            myVal += "SMTPUserName " + "auto@jahantopup.com";

                            myClient.Send(msgMail);

                        }
                    }
                }
            }
            catch (Exception x)
            {
                myVal += "x" + x.Message.ToString();
            }
            return myVal;
        }
        public static bool IsValidEmail(string email)
        {
            //regular expression pattern for valid email
            //addresses, allows for the following domains:
            //com,edu,info,gov,int,mil,net,org,biz,name,museum,coop,aero,pro,tv
            string pattern = @"^[-a-zA-Z0-9][-.a-zA-Z0-9]*@[-.a-zA-Z0-9]+(\.[-.a-zA-Z0-9]+)*\.(com|edu|info|gov|int|mil|net|org|biz|name|museum|coop|aero|pro|tv|[a-zA-Z]{2})$";
            //Regular expression object
            Regex check = new Regex(pattern, RegexOptions.IgnorePatternWhitespace);
            //boolean variable to return to calling method
            bool valid = false;

            //make sure an email address was provided
            if (string.IsNullOrEmpty(email))
            {
                valid = false;
            }
            else
            {
                //use IsMatch to validate the address
                valid = check.IsMatch(email);
            }
            //return the value to the calling method
            return valid;
        }
        #endregion

        #region Encryption/decryption

        /// <summary>
        /// The salt value used to strengthen the encryption.
        /// </summary>
        private readonly static byte[] SALT = Encoding.ASCII.GetBytes(ENCRYPTION_KEY.Length.ToString());

        public static string Encrypt(string inputText)
        {
            try
            {
                RijndaelManaged rijndaelCipher = new RijndaelManaged();
                byte[] plainText = Encoding.Unicode.GetBytes(inputText);
                PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(ENCRYPTION_KEY, SALT);

                using (ICryptoTransform encryptor = rijndaelCipher.CreateEncryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16)))
                {
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        using (CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                        {
                            cryptoStream.Write(plainText, 0, plainText.Length);
                            cryptoStream.FlushFinalBlock();
                            ////return "?" + PARAMETER_NAME + Convert.ToBase64String(memoryStream.ToArray());
                            return Convert.ToBase64String(memoryStream.ToArray());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public static string Decrypt(string inputText)
        {
            try
            {
                RijndaelManaged rijndaelCipher = new RijndaelManaged();
                byte[] encryptedData = Convert.FromBase64String(inputText);
                PasswordDeriveBytes secretKey = new PasswordDeriveBytes(ENCRYPTION_KEY, SALT);

                using (ICryptoTransform decryptor = rijndaelCipher.CreateDecryptor(secretKey.GetBytes(32), secretKey.GetBytes(16)))
                {
                    using (MemoryStream memoryStream = new MemoryStream(encryptedData))
                    {
                        using (CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                        {
                            byte[] plainText = new byte[encryptedData.Length];
                            int decryptedCount = cryptoStream.Read(plainText, 0, plainText.Length);
                            return Encoding.Unicode.GetString(plainText, 0, decryptedCount);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public static string EncryptQueryString(string inputText)
        {
            byte[] plainText = Encoding.UTF8.GetBytes(inputText);

            using (RijndaelManaged rijndaelCipher = new RijndaelManaged())
            {
                PasswordDeriveBytes secretKey = new PasswordDeriveBytes(ENCRYPTION_KEY, SALT);
                using (ICryptoTransform encryptor = rijndaelCipher.CreateEncryptor(secretKey.GetBytes(32), secretKey.GetBytes(16)))
                {
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        using (CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                        {
                            cryptoStream.Write(plainText, 0, plainText.Length);
                            cryptoStream.FlushFinalBlock();
                            string base64 = Convert.ToBase64String(memoryStream.ToArray());
                            base64 = base64.Replace("+", "zz0X0yy");

                            // Generate a string that won't get screwed up when passed as a query string.
                            string urlEncoded = HttpUtility.UrlEncode(base64);
                            //urlEncoded = urlEncoded.Replace("%2b", "%2B");
                            //string urlEncoded = base64.Replace("=", "zYz");
                            //urlEncoded = urlEncoded.Replace("/", "xYx");
                            //urlEncoded = urlEncoded.Replace("+", "zYy");

                            //javascript: OpenRadWin('/Customer/CustomerProfile.aspx?ID=1o4tIyx31D%2f%2bw38%2f%2fvDFfg%3d%3d&Tab=0')

                            return urlEncoded;
                        }
                    }
                }
            }
            //try
            //{
            //    byte[] keyData = Encoding.UTF8.GetBytes(ENCRYPTION_KEY.Substring(0, 8));
            //    DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            //    byte[] textData = Encoding.UTF8.GetBytes(inputText);
            //    MemoryStream ms = new MemoryStream();
            //    CryptoStream cs = new CryptoStream(ms,
            //      des.CreateEncryptor(keyData, _keyBytes), CryptoStreamMode.Write);
            //    cs.Write(textData, 0, textData.Length);
            //    cs.FlushFinalBlock();
            //    return GetString(ms.ToArray());
            //}
            //catch (Exception)
            //{
            //    return String.Empty;
            //}
        }

        public static string DecryptQueryString(string inputText)
        {
            try
            {
                //string MyInputText = inputText.Replace("zYz", "%");

                //string MyInputText = inputText.Replace("zYz", "=");
                //MyInputText = MyInputText.Replace("xYx", "/");
                //MyInputText = MyInputText.Replace("zYy", "+");

                string MyInputText = inputText;
                MyInputText = MyInputText.Replace("zz0X0yy", "+");
                //MyInputText = HttpUtility.UrlDecode(MyInputText);

                byte[] encryptedData = Convert.FromBase64String(MyInputText);
                PasswordDeriveBytes secretKey = new PasswordDeriveBytes(ENCRYPTION_KEY, SALT);
                using (RijndaelManaged rijndaelCipher = new RijndaelManaged())
                {
                    using (ICryptoTransform decryptor = rijndaelCipher.CreateDecryptor(secretKey.GetBytes(32), secretKey.GetBytes(16)))
                    {
                        using (MemoryStream memoryStream = new MemoryStream(encryptedData))
                        {
                            using (CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                            {
                                byte[] plainText = new byte[encryptedData.Length];
                                cryptoStream.Read(plainText, 0, plainText.Length);
                                string utf8 = Encoding.UTF8.GetString(plainText);
                                return utf8;
                            }
                        }
                    }
                }
                //try
                //{
                //    byte[] keyData = Encoding.UTF8.GetBytes(ENCRYPTION_KEY.Substring(0, 8));
                //    DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                //    byte[] textData = GetBytes(inputText);
                //    MemoryStream ms = new MemoryStream();
                //    CryptoStream cs = new CryptoStream(ms,
                //      des.CreateDecryptor(keyData, _keyBytes), CryptoStreamMode.Write);
                //    cs.Write(textData, 0, textData.Length);
                //    cs.FlushFinalBlock();
                //    return Encoding.UTF8.GetString(ms.ToArray());
                //}
                //catch (Exception)
                //{
                //    return String.Empty;
                //}
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        /// <summary>
        /// Converts a byte array to a string of hex characters
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private static string GetString(byte[] data)
        {
            StringBuilder results = new StringBuilder();

            foreach (byte b in data)
                results.Append(b.ToString("X2"));

            return results.ToString();
        }

        /// <summary>
        /// Converts a string of hex characters to a byte array
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private static byte[] GetBytes(string data)
        {
            // GetString() encodes the hex-numbers with two digits
            byte[] results = new byte[data.Length / 2];

            for (int i = 0; i < data.Length; i += 2)
                results[i / 2] = Convert.ToByte(data.Substring(i, 2), 16);

            return results;
        }

        #endregion
    }
}