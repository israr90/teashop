using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AmalakAdmin.Models
{
    public class HomeModel
    {
        public int TotalProduct { get; set; }
        public int TotalOrder { get; set; }

        public int TotalUser { get; set; }
        public int TotalBusiness { get; set; }

    }
}
