using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Dapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;

namespace AmalakAdmin.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
   

    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string BusinessName { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
        public string Town { get; set; }
        public string ZipCode { get; set; }
        public string Street { get; set; }
        public string Building { get; set; }
        public string ShopNo { get; set; }
        public string TelePhone1 { get; set; }
        public string TelePhone2 { get; set; }
        public byte UserType { get; set; }
        public bool Status { get; set; }
        public string Photo { get; set; }
        public bool VerifyIPAddress { get; set; }
        public DateTime? AddedDate { get; set; }
        public string AddedBy { get; set; }
        //public DateTime? PinCodeExpiryDate { get; set; }
        // public DateTime? DateLastLogin { get; set; }
        //public DateTime? DateOfBirth { get; set; }
        public ClaimsIdentity GenerateUserIdentity(ApplicationUserManager manager)
        {
            var userIdentity = manager.CreateIdentity(this, DefaultAuthenticationTypes.ApplicationCookie);
            return userIdentity;
        }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
        public Task<ClaimsIdentity> GenerateUserIdentityAsync(ApplicationUserManager manager)
        {
            return Task.FromResult(GenerateUserIdentity(manager));
        }
        public static ApplicationUser GetUserProfile()
        {
            if (HttpContext.Current.Request.IsAuthenticated)
            {
                var manager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
                return manager.FindById(HttpContext.Current.User.Identity.GetUserId());
            }
            else
            {
                return null;
            }
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }
        //  public DbSet<AppToken> AppTokens { get; set; }
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
        protected override void OnModelCreating(System.Data.Entity.DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            var user = modelBuilder.Entity<IdentityUser>().HasKey(u => u.Id).ToTable("Users", "dbo"); //Specify our our own table names instead of the defaults

            user.Property(iu => iu.Id).HasColumnName("UserID");
            user.Property(iu => iu.UserName).HasColumnName("UserName");
            user.Property(iu => iu.Email).HasColumnName("Email").HasMaxLength(256).IsRequired();
            user.Property(iu => iu.EmailConfirmed).HasColumnName("EmailConfirmed");
            user.Property(iu => iu.PasswordHash).HasColumnName("PasswordHash");
            user.Property(iu => iu.SecurityStamp).HasColumnName("SecurityStamp");

            user.HasMany(u => u.Roles).WithRequired().HasForeignKey(ur => ur.UserId);
            user.HasMany(u => u.Claims).WithRequired().HasForeignKey(uc => uc.UserId);
            user.HasMany(u => u.Logins).WithRequired().HasForeignKey(ul => ul.UserId);
            user.Property(u => u.UserName).IsRequired();

            var applicationUser = modelBuilder.Entity<ApplicationUser>().HasKey(au => au.Id).ToTable("Users", "dbo"); //Specify our our own table names instead of the defaults

            applicationUser.Property(au => au.Id).HasColumnName("UserID");

            applicationUser.Property(au => au.FirstName).HasColumnName("FirstName").HasMaxLength(250);
            applicationUser.Property(au => au.LastName).HasColumnName("LastName").HasMaxLength(250);
            applicationUser.Property(au => au.BusinessName).HasColumnName("BusinessName");
            applicationUser.Property(au => au.Country).HasColumnName("Country");
            applicationUser.Property(au => au.AddedBy).HasColumnName("AddedBy");
            applicationUser.Property(au => au.TelePhone1).HasColumnName("TelePhone1");
            applicationUser.Property(au => au.TelePhone2).HasColumnName("TelePhone2");
            applicationUser.Property(au => au.City).HasColumnName("City");
            applicationUser.Property(au => au.State).HasColumnName("State");
            applicationUser.Property(au => au.ZipCode).HasColumnName("ZipCode");
            applicationUser.Property(au => au.Street).HasColumnName("Street");
            // applicationUser.Property(au => au.SecondaryEmail).HasColumnName("SecondaryEmail");
            // applicationUser.Property(au => au.Fax).HasColumnName("Fax");
            applicationUser.Property(au => au.Building).HasColumnName("Building");
            applicationUser.Property(au => au.ShopNo).HasColumnName("ShopNo");
            applicationUser.Property(au => au.UserType).HasColumnName("UserType");
            applicationUser.Property(au => au.Status).HasColumnName("Status");
            applicationUser.Property(au => au.Photo).HasColumnName("Photo");
            applicationUser.Property(au => au.VerifyIPAddress).HasColumnName("VerifyIPAddress");
            //  applicationUser.Property(au => au.DateLastLogin).HasColumnName("DateLastLogin");
            applicationUser.Property(au => au.AddedDate).HasColumnName("AddedDate");
            //applicationUser.Property(au => au.PinCode).HasColumnName("PinCode");
            //applicationUser.Property(au => au.PinCodeExpiryDate).HasColumnName("PinCodeExpiryDate");

            applicationUser.Property(au => au.UserName).HasMaxLength(50).HasColumnName("UserName");
            applicationUser.Property(au => au.PasswordHash).HasColumnName("PasswordHash");
            applicationUser.Property(au => au.SecurityStamp).HasColumnName("SecurityStamp");

            var role = modelBuilder.Entity<IdentityRole>().HasKey(ir => ir.Id).ToTable("Roles", "dbo");

            role.Property(ir => ir.Id).HasColumnName("RoleID");
            role.Property(ir => ir.Name).HasColumnName("Name");

            var claim = modelBuilder.Entity<IdentityUserClaim>().HasKey(iuc => iuc.Id).ToTable("UserClaims", "dbo");

            claim.Property(iuc => iuc.Id).HasColumnName("ClaimID");
            claim.Property(iuc => iuc.ClaimType).HasColumnName("ClaimType");
            claim.Property(iuc => iuc.ClaimValue).HasColumnName("ClaimValue");
            claim.Property(iuc => iuc.UserId).HasColumnName("UserId");

            var login = modelBuilder.Entity<IdentityUserLogin>().HasKey(iul => new { iul.UserId, iul.LoginProvider, iul.ProviderKey }).ToTable("UserLogins", "dbo"); //Used for third party OAuth providers

            login.Property(iul => iul.UserId).HasColumnName("UserId");
            login.Property(iul => iul.LoginProvider).HasColumnName("LoginProvider");
            login.Property(iul => iul.ProviderKey).HasColumnName("ProviderKey");

            var userRole = modelBuilder.Entity<IdentityUserRole>().HasKey(iur => new { iur.UserId, iur.RoleId }).ToTable("UserRoles", "dbo");

            userRole.Property(ur => ur.UserId).HasColumnName("UserId");
            userRole.Property(ur => ur.RoleId).HasColumnName("RoleId");
        }
        public IEnumerable<T> Query<T>(string sql, dynamic param = null)
        {
            // Database.Connection is inherited from DbContext
            return Database.Connection.Query<T>(sql, param as object, commandTimeout: Database.CommandTimeout);
        }

        public T QueryFirst<T>(string sql, dynamic param = null)
        {
            return Database.Connection.QueryFirst<T>(sql, param as object, commandTimeout: Database.CommandTimeout);
        }

        public T QueryFirstOrDefault<T>(string sql, dynamic param = null)
        {
            return Database.Connection.QueryFirstOrDefault<T>(sql, param as object, commandTimeout: Database.CommandTimeout);
        }
    }
    public class ApplicationSignInManager : SignInManager<ApplicationUser, string>
    {
        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager) :
            base(userManager, authenticationManager)
        { }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(ApplicationUser user)
        {
            return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
        }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
        {
            return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
        }

    }
}
