﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AmalakAdmin.Startup))]
namespace AmalakAdmin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
