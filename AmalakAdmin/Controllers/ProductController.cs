using AmalakAdmin.Models;
using Amlakapp.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
//using System.DateTime;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AmalakAdmin.Controllers
{
    [Authorize]
    public class ProductController : Controller
    { 
        db_amlakEntities    db = new db_amlakEntities();
        ApplicationUser profile = ApplicationUser.GetUserProfile();
        // GET: Product
        public ActionResult Index()
        {
            //db_amlakEntities db = new db_amlakEntities();
            return View(db.Products.ToList());
        }
        
        public ActionResult Create()
        {
            //db_amlakEntities db = new db_amlakEntities();

            return View();
        }
        [HttpPost]
        
        public ActionResult CreateProduct(Product objProduct)
        {
           // db_amlakEntities db ;
            Product obj = new Product();

            if (objProduct != null)
            {
                try
                {
                    obj.ProdcutName = objProduct.ProdcutName;
                    obj.ProductPrice = objProduct.ProductPrice;
                    obj.ProductStatus = objProduct.ProductStatus;
                    obj.ProductQuantity = objProduct.ProductQuantity;
                    obj.AddedBy = profile.Id;
                    obj.AddedDate = DateTime.Now;
                    //db.Entry(obj).State = EntityState.Modified;
                    db.Products.Add(obj);
                    db.SaveChanges();
                    TempData["success"] = "You have successfully added new product";
                    return RedirectToAction("index");
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            TempData["Failed"] = "Failed added new product";
            return View("Create", objProduct);
        }
    }
}
