﻿using Amlakapp.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AmalakAdmin.Controllers
{
    [Authorize]
    public class UmsController : Controller
    {
        // GET: Ums
        public ActionResult Index()
        {
            db_amlakEntities db = new db_amlakEntities();

            return View(db.Users.ToList());
        }
        
         public ActionResult Delete(string id)
        {
            db_amlakEntities db = new db_amlakEntities();
            User obj = db.Users.Where(x => x.UserID == id).FirstOrDefault();
            if (obj != null)
            {
                db.Users.Remove(obj);
                db.SaveChanges();
                TempData["success"] = "You have successfully deleted account: "+obj.Email;
            }
            else
            {
                TempData["Failed"] = "An error occured,Please try again.";
            }
            
            return RedirectToAction("Index");
        }
        public ActionResult edit(string id)
        {
            db_amlakEntities db = new db_amlakEntities();
            User obj=db.Users.Where(x => x.UserID == id).FirstOrDefault();
            return View(obj);
        }
        [HttpPost]
        public ActionResult edit(User objUser)
        {
            db_amlakEntities db = new db_amlakEntities();
            User obj = db.Users.Where(x => x.UserID == objUser.UserID).FirstOrDefault();
            if (obj!=null){

                obj.BusinessName = objUser.BusinessName;
                obj.Country = objUser.Country;
                obj.City = objUser.City;
                obj.Town = objUser.Town;
                obj.PostCode = objUser.PostCode;
                obj.Building = objUser.Building;
                obj.TelePhone1 = objUser.TelePhone1;
                obj.TelePhone2 = objUser.TelePhone2;
                obj.ShopNo = objUser.ShopNo;
                obj.FirstName = objUser.FirstName;
                obj.LastName = objUser.LastName;
                obj.UserType = objUser.UserType;
                db.Entry(obj).State = EntityState.Modified;
                db.SaveChanges();
                TempData["success"] = "You have successfully updated";
                return RedirectToAction("Index");
            }
            return View(obj);
        }

    }
}