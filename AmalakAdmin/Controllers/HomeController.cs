using Amlakapp.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AmalakAdmin.Models;

namespace AmalakAdmin.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            db_amlakEntities db = new db_amlakEntities();
            // var today = DateTime.Now.Date;
            //var tomorrow = today.AddDays(1);

            // var dateresult = db.Orders.Where(q => q.Date >= today && q.Date < tomorrow).Count();

            //ViewBag.TotalProduct = db.Products.Count();
            //ViewBag.TotalOrder = db.Orders.Count();
            //ViewBag.TotalUser = db.Users.Count();
            //ViewBag.TotalBusiness = db.Users.Where(x => x.UserType == 50).Count();
            int TotalProduct = db.Products.Count();
            int TotalOrder = db.Orders.Count();
            int TotalUser = db.Users.Count();
            int TotalBusiness = db.Users.Where(x => x.UserType == 50).Count();
            HomeModel obj = new HomeModel();
            obj.TotalBusiness = TotalBusiness;
            obj.TotalOrder = TotalOrder;
            obj.TotalProduct = TotalProduct;
            obj.TotalUser = TotalUser;
           // User user = new User();
            //user=>products =
            return View(obj);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
