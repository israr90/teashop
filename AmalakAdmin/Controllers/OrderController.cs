using Amlakapp.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AmalakAdmin.Models;

namespace AmalakAdmin.Controllers
{
    public class OrderController : Controller
    {
        db_amlakEntities db = new db_amlakEntities();
        // GET: Order
        public ActionResult Index()
        {
            var username = "" ;

            List<Order> order = new List<Order>();
            order = db.Orders.ToList();
            foreach(var item in order)
            {
                username = db.Users.Where(x => x.UserID == item.OrderBy).Select(x=>x.FirstName).FirstOrDefault();
                item.UserName = username;
            }

            return View(order);
        }

        public ActionResult print(int orderid)
        {
            return View(db.Orders.Find(orderid));

        }
    }
}
