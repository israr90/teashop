﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Amlakapp.Core
{
    public class DBUtils
    {
        static string DefaultConnectionName = "DefaultConnection";
        #region Database functions
        public static System.Data.DataSet GetDataSet(string strSQL)
        {
            System.Data.SqlClient.SqlConnection _mainConnection = new System.Data.SqlClient.SqlConnection();
            //_mainConnection.ConnectionString = ConfigurationManager.ConnectionStrings["ConStr"].ToString();
            _mainConnection.ConnectionString = ConfigurationManager.ConnectionStrings[DefaultConnectionName].ToString();

            DataSet toReturn = new DataSet();
            try
            {
                if (strSQL != "")
                {
                    _mainConnection.Open();
                    System.Data.SqlClient.SqlCommand MyCmd = new System.Data.SqlClient.SqlCommand(strSQL, _mainConnection);
                    System.Data.SqlClient.SqlDataAdapter MyAdp = new System.Data.SqlClient.SqlDataAdapter(MyCmd);
                    MyAdp.Fill(toReturn);
                    MyCmd.Dispose();
                    MyAdp.Dispose();
                }
                else
                {
                    toReturn = null;
                }
            }
            catch
            {
                toReturn = null;
            }
            finally
            {
                _mainConnection.Close();
            }
            return toReturn;
        }
        public static System.Data.DataTable GetDataTable(string strSQL)
        {
            System.Data.SqlClient.SqlConnection _mainConnection = new System.Data.SqlClient.SqlConnection();
            _mainConnection.ConnectionString = ConfigurationManager.ConnectionStrings[DefaultConnectionName].ToString();
            DataTable toReturn = new DataTable("myDT");
            try
            {
                if (strSQL != "")
                {
                    _mainConnection.Open();
                    System.Data.SqlClient.SqlCommand MyCmd = new System.Data.SqlClient.SqlCommand(strSQL, _mainConnection);
                    System.Data.SqlClient.SqlDataAdapter MyAdp = new System.Data.SqlClient.SqlDataAdapter(MyCmd);
                    MyAdp.Fill(toReturn);
                    MyCmd.Dispose();
                    MyAdp.Dispose();
                }
                else
                {
                    toReturn = null;
                }
            }
            catch
            {
                toReturn = null;
            }
            finally
            {
                _mainConnection.Close();
            }
            return toReturn;
        }
        public static System.Data.DataTable GetDataTable(string strSQL, string ConStr)
        {
            System.Data.SqlClient.SqlConnection _mainConnection = new System.Data.SqlClient.SqlConnection();
            _mainConnection.ConnectionString = ConStr;
            DataTable toReturn = new DataTable("myDT");
            try
            {
                if (strSQL != "")
                {
                    _mainConnection.Open();
                    System.Data.SqlClient.SqlCommand MyCmd = new System.Data.SqlClient.SqlCommand(strSQL, _mainConnection);
                    System.Data.SqlClient.SqlDataAdapter MyAdp = new System.Data.SqlClient.SqlDataAdapter(MyCmd);
                    MyAdp.Fill(toReturn);
                    MyCmd.Dispose();
                    MyAdp.Dispose();
                }
                else
                {
                    toReturn = null;
                }
            }
            catch
            {
                toReturn = null;
            }
            finally
            {
                _mainConnection.Close();
            }
            return toReturn;
        }
        public static string executeSqlGetSingle(string strSQL)
        {
            System.Data.SqlClient.SqlConnection _mainConnection = new System.Data.SqlClient.SqlConnection();
            _mainConnection.ConnectionString = ConfigurationManager.ConnectionStrings[DefaultConnectionName].ToString();
            string result = "";
            try
            {
                if (strSQL != "")
                {
                    _mainConnection.Open();
                    System.Data.SqlClient.SqlCommand MyCmd = new System.Data.SqlClient.SqlCommand(strSQL, _mainConnection);
                    if (MyCmd.ExecuteScalar() == null)
                    {
                        result = "";
                    }
                    else
                    {
                        result = MyCmd.ExecuteScalar().ToString();
                    }
                }
            }
            catch
            {
                result = "";
            }
            finally
            {
                _mainConnection.Close();
            }
            return result;
        }
        public static string executeSqlGetSingle(string strSQL, string ConStr)
        {
            System.Data.SqlClient.SqlConnection _mainConnection = new System.Data.SqlClient.SqlConnection();
            _mainConnection.ConnectionString = ConStr;
            string result = "";
            try
            {
                if (strSQL != "")
                {
                    _mainConnection.Open();
                    System.Data.SqlClient.SqlCommand MyCmd = new System.Data.SqlClient.SqlCommand(strSQL, _mainConnection);
                    if (MyCmd.ExecuteScalar() == null)
                    {
                        result = "";
                    }
                    else
                    {
                        result = MyCmd.ExecuteScalar().ToString();
                    }
                }
            }
            catch
            {
                result = "";
            }
            finally
            {
                _mainConnection.Close();
            }
            return result;
        }
        public static string executeSqlGetID(string strSQL)
        {
            System.Data.SqlClient.SqlConnection _mainConnection = new System.Data.SqlClient.SqlConnection();
            _mainConnection.ConnectionString = ConfigurationManager.ConnectionStrings[DefaultConnectionName].ToString();
            string result = "";
            try
            {
                if (strSQL != "")
                {
                    _mainConnection.Open();
                    System.Data.SqlClient.SqlCommand MyCmd = new System.Data.SqlClient.SqlCommand(strSQL, _mainConnection);
                    result = MyCmd.ExecuteScalar().ToString();
                    if (result == null)
                    {
                        result = "";
                    }
                }
            }
            catch
            {
                result = "";
            }
            finally
            {
                _mainConnection.Close();
            }
            return result;

        }
        public static string executeSqlGetID(string strSQL, string ConStr)
        {
            System.Data.SqlClient.SqlConnection _mainConnection = new System.Data.SqlClient.SqlConnection();
            _mainConnection.ConnectionString = ConStr;
            string result = "";
            try
            {
                if (strSQL != "")
                {
                    _mainConnection.Open();
                    System.Data.SqlClient.SqlCommand MyCmd = new System.Data.SqlClient.SqlCommand(strSQL, _mainConnection);
                    result = MyCmd.ExecuteScalar().ToString();
                    if (result == null)
                    {
                        result = "";
                    }
                }
            }
            catch
            {
                result = "";
            }
            finally
            {
                _mainConnection.Close();
            }
            return result;
        }
        public static bool ExecuteSQL(string strSQL)
        {
            System.Data.SqlClient.SqlConnection _mainConnection = new System.Data.SqlClient.SqlConnection();
            _mainConnection.ConnectionString = ConfigurationManager.ConnectionStrings[DefaultConnectionName].ToString();
            bool result = false;
            try
            {
                if (strSQL != "")
                {
                    _mainConnection.Open();
                    System.Data.SqlClient.SqlCommand MyCmd = new System.Data.SqlClient.SqlCommand(strSQL, _mainConnection);
                    MyCmd.ExecuteNonQuery();
                    result = true;
                }
            }
            catch
            {
                result = false;
            }
            finally
            {
                _mainConnection.Close();
            }
            return result;
        }
        public static bool ExecuteSQL(string strSQL, string ConStr)
        {
            System.Data.SqlClient.SqlConnection _mainConnection = new System.Data.SqlClient.SqlConnection();
            _mainConnection.ConnectionString = ConStr;
            bool result = false;
            try
            {
                if (strSQL != "")
                {
                    _mainConnection.Open();
                    System.Data.SqlClient.SqlCommand MyCmd = new System.Data.SqlClient.SqlCommand(strSQL, _mainConnection);
                    MyCmd.ExecuteNonQuery();
                    result = true;
                }
            }
            catch
            {
                result = false;
            }
            finally
            {
                _mainConnection.Close();
            }
            return result;
        }
        public static string ExecuteProcedure(string StoredProcedure_Name, string InputParameters, string OutputParameter)
        {
            string strReturn = "";
            System.Data.SqlClient.SqlConnection _mainConnection = new System.Data.SqlClient.SqlConnection();
            _mainConnection.ConnectionString = ConfigurationManager.ConnectionStrings[DefaultConnectionName].ToString();

            System.Data.SqlClient.SqlCommand cmdToExecute = new System.Data.SqlClient.SqlCommand(StoredProcedure_Name, _mainConnection);
            cmdToExecute.CommandText = StoredProcedure_Name;
            cmdToExecute.CommandType = CommandType.StoredProcedure;

            // Use base class' connection object

            try
            {
                List<string> param = new List<string>(InputParameters.Split(','));
                List<string> parameterSplit;
                for (int i = 0; i < param.Count; i++)
                {
                    parameterSplit = new List<string>(param[i].Split('='));
                    cmdToExecute.Parameters.Add(new System.Data.SqlClient.SqlParameter("@" + parameterSplit[0], SqlDbType.NVarChar, -1, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, parameterSplit[1]));
                }
                cmdToExecute.Parameters.Add(new System.Data.SqlClient.SqlParameter("@" + OutputParameter, SqlDbType.NVarChar, -1, ParameterDirection.Output, false, 0, 0, "", DataRowVersion.Proposed, OutputParameter));
                // cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode" + OutputParameter, SqlDbType.NVarChar, -1, ParameterDirection.Output, false, 0, 0, "", DataRowVersion.Proposed, "ErrorCode"));

                _mainConnection.Open();

                int rowseffected = cmdToExecute.ExecuteNonQuery();
                strReturn = cmdToExecute.Parameters["@" + OutputParameter].Value.ToString();
            }
            catch (Exception ex)
            {
                // some error occured. Bubble it to caller and encapsulate Exception object
                throw new Exception("Error occured.", ex);
            }
            finally
            {
                _mainConnection.Close();
                cmdToExecute.Dispose();
            }
            return strReturn;
        }
        public static int ExecuteProcedure(string StoredProcedure_Name, SqlParameter[] param)
        {
            int rowsEffected = 0;

            SqlConnection _mainConnection = new System.Data.SqlClient.SqlConnection();
            _mainConnection.ConnectionString = ConfigurationManager.ConnectionStrings[DefaultConnectionName].ToString();

            SqlCommand cmdToExecute = new SqlCommand(StoredProcedure_Name, _mainConnection);
            cmdToExecute.CommandText = StoredProcedure_Name;
            cmdToExecute.CommandType = CommandType.StoredProcedure;
            cmdToExecute.Parameters.AddRange(param);
            try
            {
                _mainConnection.Open();
                rowsEffected = cmdToExecute.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception("Error Occured", ex);
            }
            finally
            {
                _mainConnection.Close();
                cmdToExecute.Dispose();
            }

            return rowsEffected;
        }
        public static bool ExecuteSQLCMD(SqlCommand cmd)
        {
            System.Data.SqlClient.SqlConnection _mainConnection = new System.Data.SqlClient.SqlConnection();
            _mainConnection.ConnectionString = ConfigurationManager.ConnectionStrings[DefaultConnectionName].ToString();
            cmd.CommandType = CommandType.Text;
            cmd.Connection = _mainConnection;
            try
            {
                _mainConnection.Open();
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                // some error occured. Bubble it to caller and encapsulate Exception object
                throw new Exception("Error occured.", ex);
            }
            finally
            {
                _mainConnection.Close();
                cmd.Dispose();
            }
        }
        #endregion
    }
}
