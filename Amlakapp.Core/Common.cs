﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Globalization;
using System.Net.Mail;

namespace Amlakapp.Core
{

    public class Common
    {

//        ApplicationUser Profile = ApplicationUser.GetUserProfile()


        //#region Telerik Grid Filtering & Sorting 
        //public static SqlBuilder ApplyFilters(SqlBuilder sqlBuilder, IEnumerable<IFilterDescriptor> filters)
        //{
        //    if (filters.Any())
        //    {
        //        foreach (var filter in filters)
        //        {
        //            var descriptor = filter as FilterDescriptor;
        //            if (descriptor != null && descriptor.Member != "")
        //            {
        //                sqlBuilder.Where(descriptor.Member + GetOperatorValue(Common.toString(descriptor.Operator)) + "'" + descriptor.Value + "'");
        //            }
        //            else if (filter is CompositeFilterDescriptor)
        //            {
        //                sqlBuilder = ApplyFilters(sqlBuilder, ((CompositeFilterDescriptor)filter).FilterDescriptors);
        //            }
        //        }
        //    }
        //    return sqlBuilder;
        //}
        //public static SqlBuilder ApplySorting(SqlBuilder sqlBuilder, IEnumerable<SortDescriptor> sorts)
        //{
        //    if (sorts != null && sorts.Any())
        //    {
        //        foreach (SortDescriptor sortDescriptor in sorts)
        //        {
        //            if (sortDescriptor.SortDirection == ListSortDirection.Descending)
        //            {
        //                sqlBuilder.OrderBy(sortDescriptor.Member + " desc");
        //            }
        //            else
        //            {
        //                sqlBuilder.OrderBy(sortDescriptor.Member);
        //            }
        //        }
        //    }
        //    return sqlBuilder;
        //}

        //public static string GetOperatorValue(string pOperator)
        //{
        //    if (pOperator == "contains")
        //    {
        //        return " Like ";
        //    }
        //    else if (pOperator == "doesnotcontain")
        //    {
        //        return " Not Like ";
        //    }
        //    else if (pOperator == "notlike")
        //    {
        //        return " Not Like ";
        //    }
        //    else if (pOperator == "like")
        //    {
        //        return " Like ";
        //    }
        //    else if (pOperator == "isnull")
        //    {
        //        return " is null";
        //    }
        //    else if (pOperator == "isnotnull")
        //    {
        //        return " is not null ";
        //    }
        //    else if (pOperator == "isequalto")
        //    {
        //        return " = ";
        //    }
        //    else if (pOperator == "notequalto")
        //    {
        //        return " <> ";
        //    }
        //    else if (pOperator == "greaterthan")
        //    {
        //        return " > ";
        //    }
        //    else if (pOperator == "greaterthanorequalto")
        //    {
        //        return " >= ";
        //    }
        //    else if (pOperator == "lessthan")
        //    {
        //        return " < ";
        //    }
        //    else if (pOperator == "lessthanorequalto")
        //    {
        //        return " <= ";
        //    }
        //    return " = ";
        //}
        //#endregion

        #region Common converstaion
        public static DateTime toDateTime(string pDate)
        {
            DateTime retDate = Convert.ToDateTime("1/1/1990 00:00:00");
            try
            {
                if (pDate.ToString() != string.Empty)
                {
                    retDate = DateTime.Parse(pDate.ToString());
                }
            }
            catch
            {
            }
            return retDate;
        }

        public static DateTime toDateTime(object pDate)
        {
            DateTime retDate = Convert.ToDateTime("1/1/1990 00:00:00");
            try
            {
                if (pDate != null)
                {
                    if (pDate.ToString() != string.Empty)
                    {
                        retDate = DateTime.Parse(pDate.ToString());
                    }
                }
            }
            catch
            {
            }
            return retDate;
        }
        public static string toString(object obj)
        {
            try
            {
                if (obj != null)
                    return obj.ToString();
                return "";
            }
            catch
            {
                return "";
            }
        }
        public static bool toBool(string str)
        {
            try
            {
                if (!isNull(str))
                {
                    string s = str.ToString();
                    s = s.Trim();
                    s = s.ToLower();

                    if (s != String.Empty)
                    {
                        if (s == "-1" || s == "1" ||
                            s == "yes" ||
                            s == "true")
                            return true;
                        else
                            return false;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }

        }
        public static bool toBool(object obj)
        {
            try
            {
                if (!isNull(obj))
                {
                    string s = obj.ToString();
                    s = s.Trim();
                    s = s.ToLower();

                    if (s != String.Empty)
                    {
                        if (s == "-1" || s == "1" ||
                            s == "yes" ||
                            s == "true")
                            return true;
                        else
                            return false;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }

        }
        public static bool isDate(object date)
        {
            try
            {
                if (date != null)
                    if (date.ToString() != string.Empty)
                    {
                        DateTime dt = DateTime.Parse(date.ToString());
                        return true;
                    }
                return false;
            }
            catch
            {
                return false;
            }

        }
        public static bool isNullDate(DateTime dt)
        {
            try
            {
                if (dt.Year == 1900 && dt.Month == 1 && dt.Day == 1)
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
        public static bool isDbNull(object obj)
        {
            try
            {
                if (obj == null)
                    return true;
                if (obj.ToString() != string.Empty && obj.ToString().Length > 0)
                    return false;
                return true;
            }
            catch
            {
                return true;
            }

        }
        public static int toInt(string s)
        {
            try
            {
                if (s != string.Empty)
                {
                    int nm = int.Parse(s);
                    return nm;
                }
                return 0;
            }
            catch
            {
                return 0;
            }

        }
        public static int toInt(object obj)
        {
            try
            {
                if (!isNull(obj))
                {
                    int nm = int.Parse(obj.ToString());
                    return nm;
                }
                return 0;
            }
            catch
            {
                return 0;
            }

        }
        public static decimal toDecimal(string s)
        {
            try
            {
                if (s != string.Empty)
                {
                    decimal nm = decimal.Parse(s);
                    return nm;
                }
                return 0;
            }
            catch
            {
                return 0;
            }

        }
        public static decimal toDecimal(object pObj)
        {
            try
            {
                if (!isNull(pObj))
                {
                    decimal nm = decimal.Parse(pObj.ToString());
                    return nm;
                }
                return 0;
            }
            catch
            {
                return 0;
            }
        }
        public static double toDouble(string s)
        {
            try
            {
                if (s != string.Empty)
                {
                    double nm = double.Parse(s);
                    return nm;
                }
                return 0;
            }
            catch
            {
                return 0;
            }

        }
        public static double toDouble(object obj)
        {
            try
            {
                if (!isNull(obj))
                {
                    double nm = double.Parse(obj.ToString());
                    return nm;
                }
                return 0;
            }
            catch
            {
                return 0;
            }

        }
        public static bool isNull(object obj)
        {
            try
            {
                if (obj == null)
                    return true;
                if (obj.ToString() != string.Empty && obj.ToString().Length > 0)
                    return false;
                return true;
            }
            catch
            {
                return true;
            }

        }

        #endregion

        


        

        


        

        


        

        #region Email functions        
        public static void SendEmail(string ToEmail, string FromEmail, string e_Subject, string e_Body)
        {
            string myVal = "";
            try
            {
                myVal += "1a";
                string smtp_host = "localhost";
                if (!string.IsNullOrEmpty("smtp.ionos.co.uk"))
                {
                   // smtp_host = "mail.iremitfy.com";
                    smtp_host = "smtp.ionos.co.uk";
                }
                myVal += "smtp_host=" + smtp_host;
                string AddressTo = ToEmail;
                string AddressFrom = FromEmail;
                string Subject = e_Subject;
                string Body = e_Body;
                if (AddressFrom != "" && AddressTo != "" && Subject != "" && Body != "" && smtp_host != "")
                {
                    char[] separator = new char[] { ',' };
                    string[] exp_list = AddressTo.Split(separator);
                    for (int i = 0; i < exp_list.Length; i++)
                    {
                        string email_to = exp_list[i].ToString().Trim();
                        bool is_valid = true;
                        bool is_valid2 = true;
                        if (is_valid == true && is_valid2 == true)
                        {
                            MailMessage msgMail = new MailMessage(AddressFrom, email_to);

                            myVal += "-AddressFrom=" + AddressFrom;
                            myVal += "-email_to=" + email_to;

                            msgMail.Subject = Subject;
                            Body = HttpUtility.HtmlDecode(Body);
                            msgMail.Body = Body;

                            msgMail.BodyEncoding = System.Text.Encoding.UTF8;
                            msgMail.IsBodyHtml = true;

                            myVal += "-Body=" + Body;
                            myVal += "-Subject=" + Subject;


                            SmtpClient myClient = new SmtpClient();

                            myClient.Host = smtp_host;
                            myVal += "Host" + smtp_host;

                           // myClient.Port = 26;
                          //  myVal += "Port" + "26";

                            myClient.Port = 587; 
                            myVal += "Port" + "587";




                            myClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                            myClient.UseDefaultCredentials = false;
                            myClient.EnableSsl = true;
                          //  myClient.Credentials = new System.Net.NetworkCredential("auto@iremitfy.com", "signaTure8170");
                         //   myVal += "SMTPUserName auto@iremitfy.com";

                            myClient.Credentials = new System.Net.NetworkCredential("password@healthkitchen.online", "Office@505");
                            myVal += "SMTPUserName password@healthkitchen.online";
                            myClient.Send(msgMail);

                            #region old code
                            //using (var client = new SmtpClient(smtp_host)
                            //{
                            //    Port = 26,
                            //    DeliveryMethod = SmtpDeliveryMethod.Network,
                            //    UseDefaultCredentials = false,
                            //    EnableSsl = true,
                            //    Credentials = new NetworkCredential("auto@sendtopup.co.uk", "(*PT3%Qea@B6")
                            //})
                            //{
                            //    using (var mail = new MailMessage(AddressFrom, email_to)
                            //    {
                            //        Subject = Subject,
                            //        Body = Body,
                            //        IsBodyHtml = true,
                            //        BodyEncoding = System.Text.Encoding.UTF8
                            //    })
                            //    {
                            //        client.Send(mail);
                            //    }
                            //}
                            #endregion
                        }
                    }
                }
            }
            catch (Exception x)
            {
                myVal += "x" + x.Message.ToString();
                //Response.Write(myVal);
            }



        }


        #endregion

    }
}
