USE [db_amlak_tea]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 1/18/2022 1:28:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[japitokens]    Script Date: 1/18/2022 1:28:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[japitokens](
	[UserID] [nvarchar](128) NOT NULL,
	[IssuedOn] [datetime] NULL,
	[ExpiresOn] [datetime] NULL,
	[AuthToken] [varchar](50) NULL,
 CONSTRAINT [PK_japitokens] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrderProducts]    Script Date: 1/18/2022 1:28:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderProducts](
	[OrderProductId] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [int] NULL,
	[ProductId] [int] NULL,
	[Quantity] [int] NULL,
	[Price] [float] NULL,
	[OrderBy] [nvarchar](128) NULL,
 CONSTRAINT [PK_OrderProducts] PRIMARY KEY CLUSTERED 
(
	[OrderProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Orders]    Script Date: 1/18/2022 1:28:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[OrderId] [int] IDENTITY(1,1) NOT NULL,
	[OrderBy] [nvarchar](128) NULL,
	[OrderStatus] [nvarchar](50) NULL,
	[OrderDate] [smalldatetime] NULL,
	[DeliveryAddress] [nvarchar](500) NULL,
	[TotalPrice] [float] NULL,
 CONSTRAINT [PK_Orders] PRIMARY KEY CLUSTERED 
(
	[OrderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Products]    Script Date: 1/18/2022 1:28:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[ProductID] [int] IDENTITY(1,1) NOT NULL,
	[ProdcutName] [nvarchar](250) NULL,
	[ProductQuantity] [int] NULL,
	[ProductStatus] [bit] NULL,
	[ProductPrice] [float] NULL,
	[AddedBy] [nvarchar](128) NULL,
	[AddedDate] [smalldatetime] NULL,
 CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED 
(
	[ProductID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Roles]    Script Date: 1/18/2022 1:28:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[RoleID] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.Roles] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserClaims]    Script Date: 1/18/2022 1:28:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserClaims](
	[ClaimID] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.UserClaims] PRIMARY KEY CLUSTERED 
(
	[ClaimID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserLogins]    Script Date: 1/18/2022 1:28:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserLogins](
	[UserId] [nvarchar](128) NOT NULL,
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.UserLogins] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[LoginProvider] ASC,
	[ProviderKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserRoles]    Script Date: 1/18/2022 1:28:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.UserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 1/18/2022 1:28:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[UserID] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NOT NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[FirstName] [nvarchar](250) NULL,
	[LastName] [nvarchar](250) NULL,
	[BusinessName] [nvarchar](max) NULL,
	[Country] [nvarchar](max) NULL,
	[State] [nvarchar](max) NULL,
	[City] [nvarchar](max) NULL,
	[PostCode] [nvarchar](max) NULL,
	[Town] [nvarchar](max) NULL,
	[ZipCode] [nvarchar](max) NULL,
	[Street] [nvarchar](max) NULL,
	[Building] [nvarchar](max) NULL,
	[ShopNo] [nvarchar](max) NULL,
	[TelePhone1] [nvarchar](max) NULL,
	[TelePhone2] [nvarchar](max) NULL,
	[UserType] [tinyint] NULL,
	[Status] [bit] NULL,
	[Photo] [nvarchar](max) NULL,
	[VerifyIPAddress] [bit] NULL,
	[AddedDate] [datetime] NULL,
	[AddedBy] [nvarchar](max) NULL,
	[Discriminator] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.Users] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
INSERT [dbo].[__MigrationHistory] ([MigrationId], [ContextKey], [Model], [ProductVersion]) VALUES (N'202112240551358_InitialCreate', N'AmalakAdmin.Models.ApplicationDbContext', 0x1F8B0800000000000400D55D5B6FE3B8157E2FD0FF20F869B7C8DA89D3194C0367178993B44127178C338BA22F0346A21D6224CA2B5199188BFD657DE84FEA5F28A92B2FA2454AB2ADC102839897EF1C1E1EF21C91E770FFF79FFFCE7E790B7CE71546310AF1F9E8647C3C722076430FE1D5F92821CB9F3E8C7EF9F9CF7F9A5D7BC19BF36BD1EE94B5A33D717C3E7A21647D3699C4EE0B0C403C0E901B8571B82463370C26C00B27D3E3E3BF4D4E4E2690428C2896E3CC3E2598A000A63FE8CF79885DB82609F0EF420FFA715E4E6B1629AA730F0218AF810BCF471701F0C1D70B2F40789CB51E39173E02949305F4972307601C1240289F679F63B8205188578B352D00FED3660D69BB25F06398F37F5635371DCAF1940D6552752CA0DC242661600978729ACB6622776F25E151293B2ABD6B2A65B261A34E25783EBAF5605AF429F4A900648267733F628DCF477725898B787D0FC9B8E838CE206F220AF72D8CBE8E79C423C7B8DF51A94BD3F131FBEFC899273E4922788E614222E01F398FC9B38FDC7FC2CD53F815E2F3D393E7E5E98777EF8177FAFEAFF0F41D3F523A56DA4E28A0458F51B88611E50D2ECBF18F9C89D86F22772CBB717D32A9505DA2CB62E4DC81B78F10AFC80B5D30D30F23E706BD41AF28C995EB33467415D14E244AE8CFFBC4F7C1B30FCBFAC9569AECDF2D54A7EFDEF742F51EBCA2553AF5127DBA7022BAAE3E413FAD8D5FD03A5B5EC27C7FC99BDD4461C07E8BFA95D57E598449E4B2C184DA264F205A412272379B54CA6BA4D20CAA7FB52E5087AFDA8C5355BD6B9BB201B5590905897DAF8682DFDDD235D6B88BF59A4E5EAA5A4C22DB144E355663A9F791C3B5A954E7C45475301DD2C8B90431CCA5C32FD294BDADA2BD41514C1A779B6323E96E9FC48F604F842E93186118C70DC4E89F3D109B87D49389363BA7B3A09AB587D1509DD93991C7302673DA7AE7849EC26F78E744FE8DD67B190C0586CC46EE98CC65827C2F85DEF5785EC2F57DB87B1D803E7C7C09313CD91FA9E9CE49B17D9D9128085D6E88BD55655B4A1297102135B2005BA3D01193DD4F23358B68B9B97DBCF0BC886EED5D99A630D0BBE27654F6F7130ACC3A5EF6BD47B6F275E73E4041FFCE6E0A3B7C6FB7DB87DC2D26A7D31AADE1C4B8206104FF0E318CA86A788F801018E16A064C56E8215CE574FAF8AD6177BE02A3F42BF093BE49B55A0D1FC315C2FDAF861476F8ABC1E2DB2F1D112D7E451EFB7431E85134A69C7C3F9F8CD230F74D5E90D9403E5B858FC2DE57CAF017C9F772F6771D00E4EFE1F0AF8EEC3CC44B1405D0EBEC168238A673EDFD03C42FBBFF96806E12510DA31E6DB0DEFDF72BF3F1EF93E079EBC6D23BADDEA6E6E95B78035CEADE5C63D6AB33DEC7D0FD1A26E41AA71EF567E2DA3AD525402FEC5CB82EFD42B8A1CA0CBDF474A6C9E96B76E41A4E920C4FAD5A1FC6A79ED6F6D378C6E597A29D7A1CCF556BCFE3F9367507F2DB384CADAD0187453B0D8759F5760EF336B61C32240306F3661AFED2DAEDEC654D6C2E342EE2387451CA55CD255D7EC5228E922E35A7F9BE453D0ECEAE47EEA8AD45EC0C9A96502336922DE703BE823E24D0B970B34BCC39885DE0A90A4B07E45930560845734E5DC7DC5F149AD49CC3887502CC56C5D4594098A8B61F6117AD81DF2825A9A7A1DFC0C65ED2906BAEE01A6246B0511226C4EBAF6A1803251D69529A24349B701A67A688FCEED234E1B55B8DE66662BFAA58B7C16938CBCF5676AA8C3582DAA336D608C384BAF64373EFEA989B12A34997EDCA70D451B2661ACEF2C38DDDABA328A87DABA3288CEF4B1D33C7C168CE252F6238CA28FA2E8733D3AA94F6AD89822406A688991749FB10DA03466A4CC2D533AB846FA4E69087F2999FF3C4F96789AC190C7C0189E836579E6BAD4739D90E22ABCF36C04AC52C408B0F9FADA8B94DB7802DBE56B6C2E67BB3056C13A202C629828AC8C761710DF5D15AB2861A7D4C94E3295542D17423DF9FC3A9D10A79E312076E2114E173582F15AD5F6BECD96AC663269F3A875403588CA55F09150ADE20A13A57CBD8D9EA2E21C947D2001663E95742B9623608A8C6F89B9AFFEEE211AD764F0BAC38AC28CD4C59379B6421D979C16CA289DD9EDD81F51AE11517CB9D97388B2C907BFED3C23EC239C830266E5C13E85C725B522261045650AA65515B1E4C23DFAE0001CF801DC6CDBD4069566B54351B7D4192B79BEA0C16DB7ED19AFD9D1B7035525030B2AA179263DCD01106CC8F496FAC6BE6BFBEBBC362EB29C9A8E6C6631EFA4980B903882B551DF508D969298F9195A808B3893406C57B5224A6B8B6A2F88D26475D183D4D54E9BCB49F2C3D844EDC85EBC90B5CE78EEA518A732665EA6B510630719D274D8EE2ED3667ED17583A57560B8C8BD9E581B86273AC2A2A9787AA4ACD91C4B05B1E4DAC31472C636B79B0B2D01C278F9DE551F2220B5E109119416A14D036842AEC9547A94ACD91B2B8561E252B31472883567990B2D046B25950AA28DAACCC46738A9853516B8A520B7EF2A052819FBCCC42BE5CCCA82065AEBC05DA548336B541AB223FE53D242BB55B152CFC535E16498DEFA947C9C33F0595CE8ACC3194E04E1E4DA934C7E5A23C7944AED812EB72538374A9DB05A8C3E8A1F4BC308D92ABC91BE1B1AE50EC4688DA2740ADCC2E6C6E8385BC8DD9DF0FCB1F1AFC9B1FBF23439947D2F02079912506177BA180717516CB46088F11568F5063B19CC5181861558B55568BBB8A7491967855D10A4F23D1FA16169BAD12DB226CB94AAD85A3A446B9081E935ADD02BB8667B9CE62CB52036184CD4BADB63343AAA35795EE7DFB6AD8B586B9692916423C06DEAB79E8E1934C7B92DA7ADAB233FB6E73A7C1309DC0B4BB9DD9E9E78B9C8BE457F8B1F5F5F8587D052C2F1FA44A698F9E5BAB54765FD34DA53418BBD50729A25DB4115B63FAF5984298BA6087F531FF3BD70CE5EC596E52522FCFA0A5B3E6597EEEDBFC988872109C3519398500A8F7B489090CC6ACC178F19B3FF71164B6B2687007305AC2986471EAA3E9F1C9547A8F64386F834CE2D8F33B3D10728B3DF8763EFADDF9A34D047EFD597273147ED12FBB30C5AF20725F40A4C6E2777864A302D58F99B53F4BAF60D85F69F191731B7FC6E8B784563C4509A4725142F5FBCFDED8C9E4ECFDFD087391DFFEEB4BD6F5C87988E89A3B738E2541B7997EF155092B6EB2AE1DB8E92B69A787296FB11E8B7E3DAF4721F1A516B5793D695095E48967447AC96929D8FC21006F3FF288ADF3563A21D6E4A6F485D78B0875B9276DB0B479271EFD49D2BC13BBC1D6E7A1B4614D9B8382B03D989C8162BE53153D4D4C154B62B1644C793B46B3668FAD2702EC06B7EE65984ECB437AFDA5DB66C0BFF0D28D2B44FA61497EA9A51318FF1A4B2720E9C5958E42E75F55E90425BF9CD28D2FE175946E72575E40E9076EDA0B9CFC92094178236D92A68BA77ACB44DAB20D0D1DE947DE9AA74A5AF0A4BC52D2D6B049AF96741A9E7410D9C905ECF5DD936EAEB0E698AFD9172E3B7206BEF1ED90161EC070BE9B942746BA192BE519110BB85E9F0AD9DBE7F3419FE038B4F6D43ECCD1DB27E4A3FAEE46CF7B536FE9BA43C9D0AD0E940E9B986B9E0E5B7319D332D1C78EEE2012CD2CD27087945C965BED03E7DB9AE772F5A763BA3BCC2167335A24D70E49C972CB7EE02CDA432899EE5673C84A669E323B241D3BB4A93C9486199BCA83A7C2AAC93DF2BCE657CFB5579E95D757EB9A65F7C3F453FC999D1664DE637D72958E4CA5275A5255933A72FA7CAE6D241B934E75A4ECC8E4967C2BADBC8D8EA026D7711BD57C6BDF4A356FA3A3AAC91F3C44EE6D6D165F5D5274C326A5895EA94DAF1E60AEAD7600C6621094511318F61DA4D4761784B03E34E14C83CF9CED2E863E97854586AC1A8D446D1EF73F40A2763746AB0A82C58562E80AD6AE6C738B97616174258E8A26D299C81D24C0A3A6F0222268095C42ABD9ED647A5591C79D5E07CFD0BBC50F095927840E1906CFBE70D2C48CF736FA691AB0C8F3EC619DBEB4D6C710289B881D7E3FE0F496A5E4FBA6E6DC4603C1BC82FC4896CD256147B3AB4D89741F6243A05C7CA533F30483B54FC1E207BC00AFB00D6F54FD3EC2157037D5B9980EA4792244B1CFAE1058452088738CAA3FFD4975D80BDE7EFE3F2CE3A7E2076C0000, N'6.2.0-61023')
INSERT [dbo].[japitokens] ([UserID], [IssuedOn], [ExpiresOn], [AuthToken]) VALUES (N'aa95b559-c5e5-4b22-9e2b-57c8584bf478', CAST(N'2022-01-04 13:48:59.000' AS DateTime), CAST(N'2022-03-04 14:32:52.000' AS DateTime), N'52c6f40c-fbe1-4146-bafd-0d82a651faaf')
SET IDENTITY_INSERT [dbo].[OrderProducts] ON 

INSERT [dbo].[OrderProducts] ([OrderProductId], [OrderId], [ProductId], [Quantity], [Price], [OrderBy]) VALUES (6, 6, 3, 51, 1400, N'aa95b559-c5e5-4b22-9e2b-57c8584bf478')
INSERT [dbo].[OrderProducts] ([OrderProductId], [OrderId], [ProductId], [Quantity], [Price], [OrderBy]) VALUES (7, 6, 3, 51, 1400, N'aa95b559-c5e5-4b22-9e2b-57c8584bf478')
SET IDENTITY_INSERT [dbo].[OrderProducts] OFF
SET IDENTITY_INSERT [dbo].[Orders] ON 

INSERT [dbo].[Orders] ([OrderId], [OrderBy], [OrderStatus], [OrderDate], [DeliveryAddress], [TotalPrice]) VALUES (6, N'aa95b559-c5e5-4b22-9e2b-57c8584bf478', N'Pending', CAST(N'2022-01-04 14:33:00' AS SmallDateTime), N'xyz', 205)
SET IDENTITY_INSERT [dbo].[Orders] OFF
SET IDENTITY_INSERT [dbo].[Products] ON 

INSERT [dbo].[Products] ([ProductID], [ProdcutName], [ProductQuantity], [ProductStatus], [ProductPrice], [AddedBy], [AddedDate]) VALUES (1, N'Cofee', 225, 1, 20, N'aa95b559-c5e5-4b22-9e2b-57c8584bf478', CAST(N'2022-01-04 12:34:00' AS SmallDateTime))
INSERT [dbo].[Products] ([ProductID], [ProdcutName], [ProductQuantity], [ProductStatus], [ProductPrice], [AddedBy], [AddedDate]) VALUES (2, N'Strin', 2, 1, 200, N'aa95b559-c5e5-4b22-9e2b-57c8584bf478', CAST(N'2022-01-04 12:34:00' AS SmallDateTime))
INSERT [dbo].[Products] ([ProductID], [ProdcutName], [ProductQuantity], [ProductStatus], [ProductPrice], [AddedBy], [AddedDate]) VALUES (3, N'Shop', 3, 1, 400, N'aa95b559-c5e5-4b22-9e2b-57c8584bf478', CAST(N'2022-01-04 12:34:00' AS SmallDateTime))
INSERT [dbo].[Products] ([ProductID], [ProdcutName], [ProductQuantity], [ProductStatus], [ProductPrice], [AddedBy], [AddedDate]) VALUES (4, N'Soap', 23, 1, 250, N'aa95b559-c5e5-4b22-9e2b-57c8584bf478', CAST(N'2022-01-04 14:16:00' AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[Products] OFF
INSERT [dbo].[Users] ([UserID], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [FirstName], [LastName], [BusinessName], [Country], [State], [City], [PostCode], [Town], [ZipCode], [Street], [Building], [ShopNo], [TelePhone1], [TelePhone2], [UserType], [Status], [Photo], [VerifyIPAddress], [AddedDate], [AddedBy], [Discriminator]) VALUES (N'c0fac6da-ba15-431d-8dba-e37a221b2627', N'waqas@admin.com', 0, N'AMz5tRltA2+MntTwPazFburVBxuTaoECY9SHMhnqCsfAVVYZQUqTnSP5X+ibOOH6Lg==', N'ceb4fc86-5025-4d48-a931-f3a3a2ee20c0', NULL, 0, 0, NULL, 1, 0, N'waqas@admin.com', N'Muhammad', N'Israr', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 99, 1, NULL, 0, NULL, NULL, N'ApplicationUser')
ALTER TABLE [dbo].[UserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserClaims_dbo.Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserClaims] CHECK CONSTRAINT [FK_dbo.UserClaims_dbo.Users_UserId]
GO
ALTER TABLE [dbo].[UserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserLogins_dbo.Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserLogins] CHECK CONSTRAINT [FK_dbo.UserLogins_dbo.Users_UserId]
GO
ALTER TABLE [dbo].[UserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserRoles_dbo.Roles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([RoleID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserRoles] CHECK CONSTRAINT [FK_dbo.UserRoles_dbo.Roles_RoleId]
GO
ALTER TABLE [dbo].[UserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserRoles_dbo.Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserRoles] CHECK CONSTRAINT [FK_dbo.UserRoles_dbo.Users_UserId]
GO
